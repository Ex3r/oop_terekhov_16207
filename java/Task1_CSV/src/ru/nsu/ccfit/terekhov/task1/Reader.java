package ru.nsu.ccfit.terekhov.task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Reader {
    
    private  String filename;
    private Set<Container> storage;
    private int amountOfAllWords;
    
    public Reader(String filename) {
       this.filename = filename;
       amountOfAllWords = 0;
       storage = new HashSet<>();
    }

    public int read() {
        File inputFile = new File(filename);
        try(Scanner input = new Scanner(inputFile).useDelimiter("^\\W")) {
            while(input.hasNext()) {
                String word = input.nextLine();
                if (word.isEmpty ()) {
                    continue;
                }
                addToStorage(word, storage);
                amountOfAllWords++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace ();
        }

        try {
                computeFrequencyForAllElements();
            } catch (ArithmeticException ex) {
                 ex.printStackTrace();
            }

        return 0;
    }

    private void addToStorage(String curString, Set<Container> storage) {
                Container tempContainer = new Container(curString);
        if (!storage.contains (tempContainer)) {
            storage.add(tempContainer);
            return;
        }

        for (Container temp : storage) {
            if (temp.getWord ().equals (curString)) {
                temp.increment ();
            }
        }
    }

    private void computeFrequencyForAllElements() throws ArithmeticException {
        storage.forEach(element -> element.updateFrequency(amountOfAllWords));
    }

    private boolean isElementInSet(String curString) {
        boolean contains = false;
        for (Container element : storage) {
           if (element.getWord().equals(curString)) {
               contains = true;
           }
        }
        return contains;
    }

    public Set<Container> getStorage() {
        return storage;
    }
    
}
