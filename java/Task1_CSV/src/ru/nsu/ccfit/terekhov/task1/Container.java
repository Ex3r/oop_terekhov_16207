package ru.nsu.ccfit.terekhov.task1;

import java.util.Objects;

public class Container implements Comparable<Container>{
    private String word;
    private Integer amount;
    private Integer frequency;

    Container(String word) {
        this.word = word;
        this.amount = 1;
        this.frequency = 0;
    }

    public String getWord() {
        return word;
    }

    public Integer getAmount() {
        return amount;
    }

    public double getFrequency() {
        return frequency;
    }

    public void increment() {
        this.amount++;
    }

    public void updateFrequency(double amountOfAllWords) throws ArithmeticException {
        frequency = (int) ((amount / amountOfAllWords) * 100);
    }

    @Override
    public int compareTo(final Container other) {
        return Integer.compare (this.getAmount (), other.getAmount ());
    }



    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Container)) {
            return false;
        }
        Container container = (Container) o;
        return Objects.equals (word, container.word);
    }


    @Override
    public int hashCode() {
        return Objects.hash(word);
    }

}
