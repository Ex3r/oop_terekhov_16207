package ru.nsu.terekhov.factory;

import ru.nsu.terekhov.view.ViewController;
import java.util.Date;

import static java.lang.Thread.sleep;


public class Dealer implements Runnable{
    private int number;
    private int sellTime;
    private static final int defaultSellTime = 50*1000;
    private static int defaultWaiting = 5000;
    private Storage<Auto> stockStore;
    private boolean logFlag;


    public Dealer(int number, Storage<Auto> stockStore, boolean logFlag) {
        this.number = number;
        this.sellTime = Integer.MAX_VALUE;
        this.stockStore = stockStore;
        this.logFlag = logFlag;
    }

    private synchronized int getSellTime() {//ISSUE странно, что значение синхронизованно читается, а изменяется нет
        return sellTime;
    }
    public int getNumber() {
        return number;
    }

    public void setSellTime() {

            double newPerformance = ViewController.getController ().getPerformanceOfDealers ();
            while (newPerformance == 0.0) {//ISSUE подобные сравнения с double потенициально опасны, два эквивалентных с точки математического вычисления числа могут отличаться где то в 15-16 знаке после запятой в аппаратной реализации
                newPerformance = ViewController.getController ().getPerformanceOfDealers ();
                try {
                    sleep (defaultWaiting);
                } catch (InterruptedException e) {
                    return;
                }
            }

            sellTime = defaultSellTime / (int) (newPerformance);


    }

    private String makeRecord(Dealer dealer, Auto auto)
    {
        String record;
        Date date = new Date();
        record = new StringBuilder ().append (date).append (" Dealer:").append (dealer.getNumber ())
                .append (" Auto: ").append (auto.getId ())
                .append (" Body: ").append (auto.getBody ().getId ())
                .append (" Engine: ").append (auto.getEngine ().getId ())
                .append (" Accessory: ").append (auto.getAccessory ().getId ())
                .toString ();
        return record;
    }

    private void buyAuto() {
        try {
            Auto temporaryAuto = stockStore.getDetail ();
            if (logFlag){
                if (temporaryAuto!= null) {

                    Logging.log (makeRecord (this, temporaryAuto));
                } else {
                    throw new Exception ();
                }
            }
        } catch (Exception e) {
            Thread.currentThread ().interrupt ();
        }
    }

    @Override
    public void run() {
        while(!Thread.currentThread ().isInterrupted ()) {
                try {
                    setSellTime ();
                    sleep (getSellTime());
                    buyAuto ();
                } catch (InterruptedException e) {
                    Thread.currentThread ().interrupt ();
                }

                ViewController.getController ().updateSoldAutos ();
        }
    }
}
