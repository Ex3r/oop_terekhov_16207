package ru.nsu.terekhov.factory;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

class MyFormatter extends Formatter {
    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        sb.append(record.getLevel()).append(':');
        sb.append(record.getMessage()).append('\n');
        return sb.toString();
    }
}

public class Logging {
    private static FileHandler fh;
    private static Logger logging = Logger.getLogger ("");

    static {
        try {
            logging.setUseParentHandlers (false);
            fh = new FileHandler ("logInfo.log", 1000000, 1);
            fh.setFormatter(new MyFormatter());
            logging.addHandler(fh);

        } catch (IOException e) {
            e.printStackTrace ();
        }
    }


    public static void log(String record)
    {
        logging.info(record);
    }
}
