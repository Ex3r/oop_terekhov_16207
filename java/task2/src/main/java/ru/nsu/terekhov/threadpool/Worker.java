package ru.nsu.terekhov.threadpool;

import ru.nsu.terekhov.factory.WorkerTask;


public class Worker implements Runnable {
    private ThreadPool threadPool;
    Worker(ThreadPool tp) {
        threadPool = tp;
    }

    @Override
    public void run() {
        while(!Thread.currentThread ().isInterrupted ()) {
            WorkerTask workerTask = threadPool.getTask ();
            workerTask.run ();
        }
    }
}


