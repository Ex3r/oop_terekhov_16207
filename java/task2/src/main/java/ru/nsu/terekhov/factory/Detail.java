package ru.nsu.terekhov.factory;

abstract class Detail {
    private String id;
    Detail(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

}

    final class Body extends Detail{//ISSUE не очень хорошо определять несколько классов в одном java файле, при импорте двух java файлов, содержащих непубличные классы с одинаковыми именами будут интересности всякие
        public Body(String id) {
            super ("body"+id);

        }

}
    final class Engine extends  Detail {
        public Engine(String id) {
            super ("engine"+id);

        }
    }

    final class Accessories extends Detail {
        Accessories(String id) {
            super ("accessory" + id);
        }
    }

    final class Auto extends Detail {
        private Body body;
        private Engine engine;
        private Accessories accessory;

        public Auto(String id, Body body, Engine engine, Accessories accessory) {
            super ("auto" + id);
            this.body = body;
            this.engine = engine;
            this.accessory = accessory;
        }

    public Body getBody() {
        return body;
    }

    public Engine getEngine() {
        return engine;
    }
    public Accessories getAccessory() {
        return accessory;
    }

}