package ru.nsu.terekhov.factory;

import ru.nsu.terekhov.threadpool.ThreadPool;

public class StockController implements Runnable {
    private Storage<Auto> stockStorage;
    private Storage<Body> bodyStorage;
    private Storage<Engine> engineStorage;
    private Storage<Accessories> accessoriesStorage;
    private ThreadPool threadPool;
    private static final double createAutosWhen = 60;
    StockController(Storage<Body> bs, Storage<Engine> es, Storage<Accessories> as,  Storage<Auto> stock, ThreadPool tp) {
        stockStorage = stock;
        bodyStorage = bs;
        engineStorage = es;
        accessoriesStorage = as;
        threadPool = tp;
    }

    private synchronized double getFillPercentage(int taskSize) {
        //добавил taskSize, потому что иначе создаётся очень много задач и они продолжают выполняться, независимо от проверки на заполненность
        return ((double) stockStorage.getCurrentSize () + (double) taskSize) / (double) stockStorage.getCapacity () * 100;
    }

    private void analyseStockStore() {
        while (getFillPercentage (threadPool.getAmountOfTasks ()) <= createAutosWhen) {
            System.out.println ("текущая заполненность склада авто "+ getFillPercentage (threadPool.getAmountOfTasks ()));
            WorkerTask newAuto = new WorkerTask (bodyStorage.getDetail (),engineStorage.getDetail () ,accessoriesStorage.getDetail (), stockStorage);
            threadPool.addTask (newAuto);
        }
    }

    @Override
    public void run() {
        while(!Thread.currentThread ().isInterrupted ()) {
            analyseStockStore();
            stockStorage.waiting ();
        }
    }
}
