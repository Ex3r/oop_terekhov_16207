package ru.nsu.terekhov.factory;
import javafx.application.Platform;
import ru.nsu.terekhov.threadpool.ThreadPool;
import ru.nsu.terekhov.view.ViewController;

import java.util.ArrayList;
import java.util.List;


public class Factory  {
    private static int storageBodySize;
    private static int storageEngineSize;
    private static int storageAccessorySize;
    private static int storageAutoSize;
    private static int amountOFAccessorySuppliers;
    private static int amountOFWorkers;
    private static int amountOFDealers;
    private static boolean isLog;

    private BodyProducer bodyProducer;
    private EngineProducer engineProducer;
    private AccessoryProducer[] accesoryProducers;

    private Storage<Body> bodyStorage;
    private Storage<Engine> engineStorage;
    private Storage<Accessories> accessoriesStorage;
    private Storage<Auto> autoStorage;

    private Dealer[] dealers;
    private ThreadPool threadPool;
    private StockController stockController;

    //потоки
    private Thread threadBodyProd;
    private Thread threadEngineProd;
    private List<Thread> threadsAccsessoryProd;
    private List<Thread> threadsDealers;
    private Thread threadStockController;



    public Factory(String configFilename) {
        ConfigParser config = new ConfigParser ();
        config.parseConfigFile (configFilename);
        initializeValuesFromConfig (config);

        bodyStorage = new Storage<> (storageBodySize);
        engineStorage = new Storage<> (storageEngineSize);
        accessoriesStorage = new Storage<> (storageAccessorySize);
        autoStorage = new Storage<> (storageAutoSize);

        this.bodyProducer = new BodyProducer (bodyStorage);
        this.engineProducer = new EngineProducer (engineStorage);

        accesoryProducers = new AccessoryProducer[amountOFAccessorySuppliers];
        for (int i = 0; i < amountOFAccessorySuppliers; i++) {
            AccessoryProducer newAccsessoryProd = new AccessoryProducer ("AccsessoryProd" + i, accessoriesStorage);
            accesoryProducers[i] = newAccsessoryProd;
        }

        threadPool = new ThreadPool (amountOFWorkers);
        this.stockController = new StockController (bodyStorage, engineStorage, accessoriesStorage,autoStorage, threadPool);

        dealers = new Dealer[amountOFDealers];
        for (int i = 0; i < amountOFDealers; i++) {
            Dealer newDealer = new Dealer (i,autoStorage,isLog);
            dealers[i] = newDealer;
        }

        threadBodyProd = new Thread (bodyProducer, "threadBodyProd");
        threadEngineProd  = new Thread (engineProducer, "threadEngineProd");
        threadsAccsessoryProd = new ArrayList<> (amountOFAccessorySuppliers);
        threadsDealers = new ArrayList<> (amountOFDealers);
        threadStockController = new Thread (stockController ,"threadStockController");

        for (int i = 0; i < amountOFAccessorySuppliers; i++) {
            Thread tempAccThread = new Thread (accesoryProducers[i], "threadsAccessoryProducer" +i);
            threadsAccsessoryProd.add(tempAccThread);
        }

        for (int i = 0; i < amountOFDealers; i++) {
            Thread tempDealerThread = new Thread (dealers[i], "threadDealer" + i );
            threadsDealers.add(tempDealerThread);
        }


    }

    private void initializeValuesFromConfig(ConfigParser config) {
        storageBodySize = config.getStorageBodySize ();
        storageEngineSize = config.getStorageEngineSize ();
        storageAccessorySize  = config.getStorageAccessorySize ();
        storageAutoSize = config.getStorageAutoSize ();
        amountOFAccessorySuppliers = config.getAccessorySuppliers ();
        amountOFWorkers = config.getWorkers ();
        amountOFDealers = config.getDealers ();
        isLog = (config.getLogSale () != 0);
        Platform.runLater (()-> {
            ViewController.getController ().setStorageBodySize (storageBodySize);
            ViewController.getController ().setStorageEngineSize (storageEngineSize);
            ViewController.getController ().setStorageAccessorySize (storageAccessorySize);
            ViewController.getController ().setStorageAutoSize (storageAutoSize);
        });
    }

    public void run() {

        //запуск производителей и дилеров
        threadBodyProd.start ();
        threadEngineProd.start ();
        threadsAccsessoryProd.forEach (Thread::start);
        threadsDealers.forEach (Thread::start);
        threadStockController.start ();

    }


    public void stop() {
        threadPool.stop ();
        threadBodyProd.interrupt ();
        threadEngineProd.interrupt ();
        threadsAccsessoryProd.forEach (Thread::interrupt);
        threadsDealers.forEach (Thread::interrupt);
        threadStockController.interrupt ();



        System.out.println (threadBodyProd.getName () +  threadBodyProd.isInterrupted ());
        System.out.println (threadEngineProd.getName() + threadEngineProd.isInterrupted ());
        threadsAccsessoryProd.forEach (t -> System.out.println (t.getName() + t.isInterrupted ()));
        threadsDealers.forEach (t -> System.out.println (t.getName() + t.isInterrupted ()));
        System.out.println (threadStockController.getName () +threadStockController.isInterrupted ());


    }
}
