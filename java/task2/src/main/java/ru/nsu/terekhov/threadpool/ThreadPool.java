package ru.nsu.terekhov.threadpool;

import ru.nsu.terekhov.factory.WorkerTask;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ThreadPool {
    private List<Thread> threads;
    private Queue<WorkerTask> tasks;

    public ThreadPool(int poolSize) {
        threads = new ArrayList<> ();
        tasks = new LinkedList<> ();
        for(int i = 0; i < poolSize; i++) {
            Worker tempWorker = new Worker (this);
            Thread tempThread  = new Thread (tempWorker);
            threads.add (tempThread);
        }
        for(Thread thread : threads) {
            thread.start ();
        }
    }

    public synchronized WorkerTask getTask() {
            while (tasks.isEmpty ()) {
                try {
                    wait ();
                } catch (InterruptedException e) {
                    //e.printStackTrace ();
                    Thread.currentThread ().interrupt ();
                }
            }
            notify ();
            return tasks.remove();

    }

    public synchronized int getAmountOfTasks() {
        return tasks.size();
    }
    public synchronized void addTask (WorkerTask task) {
            tasks.add(task);
            notify();

    }

    public void stop () {
        for(Thread thread : threads) {
            thread.interrupt ();
        }
    }
}
