package ru.nsu.terekhov.factory;
import javafx.application.Platform;
import ru.nsu.terekhov.view.ViewController;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

public abstract class Producer<T extends Detail> implements Runnable {
    int manufacturingTime;//время , необходимое на производство одной детали
    static int timeForOneDetail = 1000 * 50;
    static int defaultWaiting = 5000;
    Storage<T> store;
    String typeName;
    AtomicInteger produced;
    protected abstract void produce(String uniqueId);
    protected abstract void setManufacturingTime();


    Producer(Storage<T> storage) {
        store = storage;
        produced = new AtomicInteger (0);
        this.manufacturingTime = Integer.MAX_VALUE;
    }

    int getManufacturingTime() {
        return manufacturingTime;
    }

    @Override
    public void run() {
        while (!Thread.currentThread ().isInterrupted ()) {
            String uniqueId = Integer.toString (produced.get ());
            try {
                setManufacturingTime ();
                sleep (getManufacturingTime ());
                produce (uniqueId);
            } catch (InterruptedException e) {
                Thread.currentThread ().interrupt ();
            }

        }
    }
}

final class BodyProducer extends Producer<Body> {
    private Body body;
    BodyProducer(Storage<Body> store) {
        super (store);
    }

    @Override
    public void produce(String uniqueId) {
        body = new Body (uniqueId);
        store.addDetail (body);
        produced.getAndIncrement ();
        typeName = ViewController.getController ().getSimpleName (Body.class.getTypeName ());
        Platform.runLater (() -> ViewController.getController ().updateTextProduced (produced.get (), typeName));
        System.out.println ("Произведён кузов №" + produced.get());

    }

    @Override
    protected void setManufacturingTime() {
        double newPerformance = ViewController.getController ().getPerformanceOfBodyProd ();
        while (newPerformance == 0.0) {
            newPerformance = ViewController.getController ().getPerformanceOfBodyProd ();
            try {
                sleep (defaultWaiting);
            } catch (InterruptedException e) {
                Thread.currentThread ().interrupt ();
            }
        }
        manufacturingTime = timeForOneDetail / (int) (newPerformance);
    }
}

final class EngineProducer extends Producer<Engine> {
    private Engine engine;
    EngineProducer(Storage<Engine> store) {
        super (store);
    }

    @Override
    public void produce(String uniqueId) {
        engine = new Engine (uniqueId);
        store.addDetail (engine);
        produced.getAndIncrement ();
        typeName = ViewController.getController ().getSimpleName (Engine.class.getTypeName ());
        Platform.runLater (() -> ViewController.getController ().updateTextProduced (produced.get (), typeName));
        System.out.println ("Произведён двигатель №" + produced);
    }

    protected void setManufacturingTime() {
        try {
        double newPerformance = ViewController.getController ().getPerformanceOfEngineProd ();
        while (newPerformance == 0.0) {
            newPerformance = ViewController.getController ().getPerformanceOfEngineProd ();
            sleep(defaultWaiting);

        }
        manufacturingTime = timeForOneDetail /  (int) (newPerformance);
        } catch (InterruptedException e) {
            Thread.currentThread ().interrupt ();
        }

    }
}


final class AccessoryProducer extends Producer<Accessories> {
    private Accessories accessory;
    private String brand;
    private static final AtomicInteger produced = new AtomicInteger (0);
    private static final Object syncObjAccessoryProd = new Object ();

    public AccessoryProducer(String brand, Storage<Accessories> store) {
        super (store);
        this.brand = brand;
    }

    @Override
    public void produce(String uniqueId) {
        synchronized (syncObjAccessoryProd) {
            accessory = new Accessories (brand + "№:" + Integer.toString (produced.get ()));
            store.addDetail (accessory);
            produced.getAndIncrement ();
            typeName = ViewController.getController ().getSimpleName (Accessories.class.getTypeName ());
            Platform.runLater (() -> ViewController.getController ().updateTextProduced (produced.get (), typeName));
            System.out.println ("Произведён аксессуар №" + produced);
        }
    }

    @Override
    protected void setManufacturingTime() {
        try {
        double newPerformance = ViewController.getController ().getPerformanceOfAccessoryProd ();
        while (newPerformance == 0.0) {
            newPerformance = ViewController.getController ().getPerformanceOfAccessoryProd ();
            sleep (defaultWaiting);
        }
        manufacturingTime = timeForOneDetail /  (int) (newPerformance);
        }
        catch (InterruptedException e) {
            Thread.currentThread ().interrupt ();
        }

    }

}



