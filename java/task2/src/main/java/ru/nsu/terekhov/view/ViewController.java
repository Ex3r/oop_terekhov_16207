package ru.nsu.terekhov.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

public class ViewController implements Initializable {

    //слайдеры
    @FXML
    private Slider bodyProdPerformance;
    @FXML
    private Slider engineProdPerformance;
    @FXML
    private Slider accessoryProdPerformance;
    @FXML
    private Slider dealersPerformance;
    //сколько всего деталей было произведено
    @FXML
    private Text textProducedBody;
    @FXML
    private Text textProducedEngine;
    @FXML
    private Text textProducedAccessory;
    @FXML
    private Text textProducedAuto; 
    @FXML
    private Text textSoldAutos; 
    //Прогресс бары
    @FXML
    private ProgressBar progressBodyStore;
    @FXML
    private ProgressBar progressEngineStore;
    @FXML
    private ProgressBar progressAccessoryStore;
    @FXML
    private ProgressBar progressAutoStore;
    //сколько сейчас на складе
    @FXML
    private Text textBodyStoreSize;
    @FXML
    private Text textEngineStoreSize;
    @FXML
    private Text textAccessoryStoreSize;
    @FXML
    private Text textAutoStoreSize;

    private static ViewController controller;

    /*для вызова метода в фабрике*/
    public static ViewController getController() {
        return controller;
    }

    /***********для конфигурации слайдеров*********/
    private static double performanceOfBodyProd;
    public double getPerformanceOfBodyProd() {
        return performanceOfBodyProd;
    }
    public void changeBodyManufactoringTime(MouseEvent mouseEvent) {
        performanceOfBodyProd = bodyProdPerformance.getValue ();
    }
    
    
    private static  double performanceOfEngineProd;
    public double getPerformanceOfEngineProd() {
        return performanceOfEngineProd;
    }
    public void changeEngineManufactoringTime(MouseEvent mouseEvent) {
        performanceOfEngineProd = engineProdPerformance.getValue ();
    }
    
    
    private static  double performanceOfAccessoryProd;
    public double getPerformanceOfAccessoryProd() {
        return performanceOfAccessoryProd;
    }
    public void changeAccessoryManufactoringTime(MouseEvent mouseEvent) {
        performanceOfAccessoryProd = accessoryProdPerformance.getValue ();
    }
    
    
    private static double performanceOfDealers;
    public double getPerformanceOfDealers() {
        return performanceOfDealers;
    }
    public void changeDealersPerformance(MouseEvent mouseEvent) {
        performanceOfDealers = dealersPerformance.getValue ();
    }
    /****************************************************************/

    /*setup constants from Factory*/
    private static int storageBodySize = 0;
    private static int storageEngineSize = 0;
    private static int storageAccessorySize =0;
    private static int storageAutoSize =0;
    public  void setStorageBodySize(int storageBodySize) {
        ViewController.storageBodySize = storageBodySize;
    }

    public  void setStorageEngineSize(int storageEngineSize) {
        ViewController.storageEngineSize = storageEngineSize;
    }

    public  void setStorageAccessorySize(int storageAccessorySize) {
        ViewController.storageAccessorySize = storageAccessorySize;
    }

    public void setStorageAutoSize(int storageAutoSize) {
        ViewController.storageAutoSize = storageAutoSize;
    }

    private static final AtomicInteger amountOfSoldAutos = new AtomicInteger (0);
    private static final AtomicInteger amountOfProducedAutos = new AtomicInteger (0);

    public synchronized void updateTextProduced(int produced, String typeName) {
        String producedInStr = Integer.toString (produced);
        switch(typeName) {
            case "Body": {
                textProducedBody.setText (producedInStr);
                break;
            }
            case "Engine": {
                textProducedEngine.setText (producedInStr);
                break;
            }

            case "Accessories": {
                textProducedAccessory.setText (producedInStr);
                break;
            }
        }
    }

    public synchronized void  updateSoldAutos() {
        amountOfSoldAutos.getAndIncrement ();
        textSoldAutos.setText (Integer.toString (amountOfSoldAutos.get()));
    }
    
    public synchronized void updateProducedAutos() {
        amountOfProducedAutos.getAndIncrement ();
        textProducedAuto.setText (Integer.toString (amountOfProducedAutos.get() ));
    }

    public synchronized void updateProgressesAndText (int storeSize, String typeName) {
        String producedInStr = Integer.toString (storeSize);
        double progressValue;
        switch(typeName) {
            case "Body": {
                progressValue = (double) storeSize / (double) storageBodySize;
                textBodyStoreSize.setText (new StringBuilder ().append (producedInStr).append ("/").append (storageBodySize).toString ());
                progressBodyStore.setProgress (progressValue);
                break;
            }
            case "Engine": {
                progressValue = (double) storeSize / (double) storageEngineSize;
                textEngineStoreSize.setText (new StringBuilder ().append (producedInStr).append ("/").append (storageEngineSize).toString ());
                progressEngineStore.setProgress (progressValue);
                break;
            }

            case "Accessories": {
                progressValue = (double) storeSize / (double) storageAccessorySize;
                textAccessoryStoreSize.setText (new StringBuilder ().append (producedInStr).append ("/").append (storageAccessorySize).toString ());
                progressAccessoryStore.setProgress (progressValue);
                break;
            }

            case "Auto": {
                progressValue = (double) storeSize / (double) storageAutoSize;
                textAutoStoreSize.setText (new StringBuilder ().append (producedInStr).append ("/").append (storageAutoSize).toString ());
                progressAutoStore.setProgress (progressValue);
                break;
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        controller = this;
    }

    /*Получение имени класса юез пакета, потому что стандартная не работает*/
    public String getSimpleName(String fullName) {
        return fullName.substring(fullName.lastIndexOf('.') + 1);
    }


}