package ru.nsu.terekhov.factory;

import javafx.application.Platform;
import ru.nsu.terekhov.view.ViewController;

import java.util.LinkedList;
import java.util.Queue;

public class Storage<T extends Detail> {
    private Queue<T> storage;
    private int capacity;


    Storage(int capacity) {
        this.storage = new LinkedList<> ();
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public synchronized void waiting () {
        try {
            wait ();
        } catch (InterruptedException e) {
            e.printStackTrace ();
        }
    }
    public synchronized int getCurrentSize() {
            return storage.size();
    }

    public synchronized void addDetail(T detail) {//ISSUE может быть дедлок при нулевых задержках, небольшом размере склада (допустим 10) и количестве воркеров (допустим 100)
            while(getCurrentSize ()  > capacity -1) {
                System.out.println ("склад переполнен");
                try {
                    wait ();
                } catch (InterruptedException e) {
                    return;
                }
            }

            storage.add (detail);
            String typeName = ViewController.getController ().getSimpleName (detail.getClass ().getTypeName ());//ISSUE Рассказывал про паттерн Observable, чтобы не засорять модель зависимостями от view
            Platform.runLater (()-> ViewController.getController ().updateProgressesAndText (getCurrentSize (), typeName));//ISSUE плюс опять вернулись к процедурному программированию с типом, вместо использования полиморфизма
            notify();
    }

    public synchronized T getDetail() {
        while (getCurrentSize () == 0) {
            System.out.println ("склад пуст ждём");
            try {
               wait ();
            } catch (InterruptedException e) {
               return null;
            }
        }

        System.out.println ("забрали деталь со склада");
        String typeName = ViewController.getController ().getSimpleName ( storage.getClass ().getTypeName ());
        Platform.runLater (()-> ViewController.getController ().updateProgressesAndText (getCurrentSize (), typeName));
        notify();
        return storage.poll();
    }

}
