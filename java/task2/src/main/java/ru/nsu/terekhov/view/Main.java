package ru.nsu.terekhov.view;
    import javafx.application.Application;
    import javafx.event.EventHandler;
    import javafx.fxml.FXMLLoader;
    import javafx.scene.Parent;
    import javafx.scene.Scene;
    import javafx.stage.Stage;
    import javafx.stage.WindowEvent;
    import ru.nsu.terekhov.factory.Factory;

    import java.io.IOException;
    import java.util.Objects;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) {
        Parent root = null;
        try {
            root = FXMLLoader.load (Objects.requireNonNull (getClass ().getClassLoader ().getResource ("mainWindow.fxml")));
        } catch (IOException e) {
            e.printStackTrace ();
        }
        primaryStage.setTitle ("Завод");
        Scene scene = new Scene (root, 800, 600);
        primaryStage.setScene (scene);
        primaryStage.setResizable (false);
        primaryStage.show ();

        //main
        final String configFilename = "config.txt";
        final  Factory factory  = new Factory (configFilename);
        factory.run ();

        primaryStage.setOnCloseRequest(we -> {
            System.out.println("Stage is closing");
            factory.stop();
            primaryStage.close();
            System.exit (0);
        });




    }

    public static void main(String[] args) {
        launch (args);
    }
}
