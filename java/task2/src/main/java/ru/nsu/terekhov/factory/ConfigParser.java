package ru.nsu.terekhov.factory;

//TODO заменить это всё на Properties
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import static java.lang.Integer.*;

public class ConfigParser {
    private static Map<String , Integer> config;

    ConfigParser() {
        config = new HashMap<> ();
    }

    public Map<String , Integer> parseConfigFile(String filename) {

        try {
            Files.lines (Paths.get (filename))
                    .map (string -> string.split ("\n"))
                    .flatMap (Arrays::stream)
                    .filter (string -> !string.isEmpty ())
                    .forEach (string -> {
                try {
                    if (string.split ("=").length != 2) {
                        throw new IOException ("Ошибочная строка в конфиге");
                    } else {
                        int value = parseInt (string.split ("=")[1]);
                        config.put (string.split ("=")[0].replaceAll("\\s",""), value);
                    }
                } catch (IOException | NumberFormatException e) {
                    e.printStackTrace ();
                }
            });
        } catch (IOException e) {
            e.printStackTrace ();
        }
        /*some check*/
        if (config.size () != 8) {
            return null;
        }
        return config;
    }

    public int getStorageBodySize()
    {
        return config.get("StorageBodySize");
    }
    public int getStorageEngineSize()
    {
        return config.get("StorageMotorSize");
    }
    public int getStorageAccessorySize()
    {
        return config.get("StorageAccessorySize");
    }
    public int getStorageAutoSize()
    {
        return config.get("StorageAutoSize");
    }
    public int getAccessorySuppliers()
    {
        return config.get("AccessorySuppliers");
    }
    public int getWorkers()
    {
        return config.get("Workers");
    }
    public int getDealers()
    {
        return config.get("Dealers");
    }
    public int getLogSale()
    {
        return config.get("LogSale");
    }

}
