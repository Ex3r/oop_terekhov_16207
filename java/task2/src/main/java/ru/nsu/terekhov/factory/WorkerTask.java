package ru.nsu.terekhov.factory;

import javafx.application.Platform;
import ru.nsu.terekhov.view.ViewController;

public class WorkerTask implements Runnable {
    private static int producedAutos = 0;
    private Body body;
    private Engine engine;
    private Accessories accessory;
    private Storage<Auto> stockStorage;

    public WorkerTask(Body body, Engine engine, Accessories accessory,Storage<Auto> stockStorage) {
        this.body = body;
        this.engine = engine;
        this.accessory = accessory;
        this.stockStorage = stockStorage;
        producedAutos++;
    }

    @Override
    public void run() {
        String newId = Integer.toString (producedAutos);
        stockStorage.addDetail (new Auto (newId, body, engine ,accessory));
        Platform.runLater (()-> ViewController.getController ().updateProducedAutos ());
        System.out.println ("Собрали машину №" + producedAutos);
    }
}


