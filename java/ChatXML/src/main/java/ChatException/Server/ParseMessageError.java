package ChatException.Server;

public class ParseMessageError extends ServerError {
    public ParseMessageError(String message) {
        super ("Server error while parsing: " + message);
    }
}
