package ChatException.Server;

public class IncorrectData extends ServerError {
    public IncorrectData(String message) {
        super (message);
    }
}
