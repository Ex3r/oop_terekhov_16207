package ChatException.Server;

public class ServerError extends Throwable {
    public ServerError(String message) {
        super (message);
    }
}
