package General;

import org.w3c.dom.Document;
import java.io.DataOutputStream;

public interface AbstractSender {
    void sendMessage(Document message, DataOutputStream outputStream);
}
