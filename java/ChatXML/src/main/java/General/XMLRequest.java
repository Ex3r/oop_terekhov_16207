package General;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.util.ArrayList;


public class XMLRequest {
    private  final static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private  Document doc;
    private Element root;
    private ArrayList<XMLItem> items;

    public static void main(String[] args) {
       /* XMLRequest msg = new XMLRequest ("command", "name", "login",null);
        msg.appendMessage ("name", "command", null, null, null);
        msg.appendMessage ("type", "command", null, null, "CHAT_CLIENT_NAME");
        msg.appendMessage ("inname", "name", null, null, "USER_NAME");
        msg.printToFile ();*/
    }

    public XMLRequest(String tag, String attribute, String valueOfAttribute, String text) {
        factory.setNamespaceAware(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setValidating (true);
        try {
            items = new ArrayList<> ();
            doc = factory.newDocumentBuilder ().newDocument ();
            root = doc.createElement (tag);
            XMLItem temp = new XMLItem (root, tag,attribute, valueOfAttribute, text);
            if (!temp.isEmptyAttr ()) {
                root.setAttribute (attribute,valueOfAttribute);
            }
            doc.appendChild (root);
            items.add (temp);
        } catch (ParserConfigurationException e) {
            e.printStackTrace ();
        }
    }

    private XMLItem findElementByTagName(String tag) {
        for(XMLItem item : items){
            if(item.getTag () != null && item.getTag().contains(tag)) {
                return item;
            }
        }
        return null;
    }

    public void appendMessage(String tag, String insertInTag, String attribute, String valueOfAttribute, String text) {
        XMLItem insertAfterThis = findElementByTagName (insertInTag);
        XMLItem newItem;
        Element newElement = doc.createElement (tag);
        if (insertAfterThis != null) {
            newItem = insertAfterThis.addChild (newElement,tag, attribute, valueOfAttribute, text);
        }
        else {
            newItem = new XMLItem (newElement,tag,attribute,valueOfAttribute,text);
            if (!newItem.isEmptyAttr ()) {
                newElement.setAttribute (attribute,valueOfAttribute);
            }
            doc.appendChild (newItem.getElement ());
        }

        items.add (newItem);
    }

    /*для отладки*/
   public void printToFile() {
       removeEmptyStrings ();
       File file = new File("test.xml");
       Transformer transformer;
       try {

           transformer = TransformerFactory.newInstance().newTransformer();
           transformer.setOutputProperty(OutputKeys.INDENT, "yes");
           transformer.transform(new DOMSource (doc), new StreamResult (file));
       } catch (TransformerException e) {
           e.printStackTrace ();
       }
   }

    private void removeEmptyStrings () {
        XPath xp = XPathFactory.newInstance().newXPath();
        NodeList nl = null;
        try {
            nl = (NodeList) xp.evaluate("//text()[normalize-space(.)='']", doc, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace ();
        }

        if (nl != null) {
            for (int i=0; i < nl.getLength(); ++i) {
                Node node = nl.item(i);
                node.getParentNode().removeChild(node);
            }
        }
    }


    public  Document getDoc() {
        return doc;
    }
}
