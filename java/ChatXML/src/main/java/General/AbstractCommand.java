package General;

import org.w3c.dom.Element;

public abstract class AbstractCommand {
    public abstract void executeCommand(Element element);
}
