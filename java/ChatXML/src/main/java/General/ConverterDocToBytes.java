package General;

import org.w3c.dom.Document;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;

public class ConverterDocToBytes {
    public static ByteArrayOutputStream convertMsgToOut(Document doc) {
        ByteArrayOutputStream byteMessage = new ByteArrayOutputStream ();
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource (doc), new StreamResult (byteMessage));
        } catch (TransformerException e) {
            e.printStackTrace ();
        }


        return byteMessage;
    }
}
