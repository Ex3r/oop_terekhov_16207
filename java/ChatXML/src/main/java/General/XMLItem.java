package General;
import org.w3c.dom.Element;
import java.util.Optional;

public class XMLItem {
    private String tag;
    private Element root;
    private boolean emptyAttr;
    public static void main(String[] args) {

    }

    public XMLItem(Element insertNode, String tag, String attribute, String valueOfAttribute, String text) {
        this.root = insertNode;
        this.tag = tag;

        attribute = Optional.ofNullable (attribute).orElse ("");
        valueOfAttribute = Optional.ofNullable (valueOfAttribute).orElse ("");
        text = Optional.ofNullable (text).orElse (" ");
        insertNode.setTextContent (text);
        emptyAttr = (attribute.equals ("")) && (valueOfAttribute.equals (""));
    }

    public XMLItem addChild(Element newElement, String tag, String attribute, String valueOfAttribute, String text) {
            attribute = Optional.ofNullable (attribute).orElse ("");
            valueOfAttribute = Optional.ofNullable (valueOfAttribute).orElse ("");
            text = Optional.ofNullable (text).orElse (" ");
            newElement.setTextContent (text);
            if ((attribute.equals ("")) && (valueOfAttribute.equals (""))) {
                root.appendChild (newElement);
            }
            else {
                newElement.setAttribute (attribute,valueOfAttribute);
                root.appendChild (newElement);
            }
            return new XMLItem (newElement, tag, attribute , valueOfAttribute, text);
    }

    public boolean isEmptyAttr() {
        return  emptyAttr;
    }

    public Element getElement () {
        return root;
    }
    public String getTag() {
        return tag;
    }


}
