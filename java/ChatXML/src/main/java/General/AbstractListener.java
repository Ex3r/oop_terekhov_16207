package General;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

public interface AbstractListener {
    ByteArrayInputStream getMessage(DataInputStream input);
}
