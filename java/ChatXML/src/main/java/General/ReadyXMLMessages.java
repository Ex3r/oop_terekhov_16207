package General;
import Server.ServerObserver;
import org.w3c.dom.Document;
import java.util.ArrayList;
import java.util.List;

public class ReadyXMLMessages {
    private  enum Tags {
        LOGIN("login"),
        NAME("name"),
        COMMAND("command"),
        TYPE("type"),
        SUCCESS("success"),
        SESSION("session"),
        ERROR("error"),
        MESSAGE("message"),
        LIST("list"),
        LISTUSERS("listusers"),
        USER("user"),
        EVENT("event"),
        LOGOUT("logout"),
        USERLOGIN("userlogin"),
        USERLOGOUT("userlogout") {};

        private String tag;
        Tags(String tag) {
            this.tag = tag;
        }
        public String getStr() {
            return tag;
        }
    }
    //1Registration
    public Document ClientRequestRegistration (String textUserName,String textChatClientName) {
        XMLRequest msg = new XMLRequest (Tags.COMMAND.getStr () ,Tags.NAME.getStr (),Tags.LOGIN.getStr (), null);
        msg.appendMessage (Tags.NAME.getStr (),Tags.COMMAND.getStr (), null, null, textUserName);
        msg.appendMessage (Tags.TYPE.getStr () , Tags.COMMAND.getStr (), null, null, textChatClientName);
        return msg.getDoc ();
    }

    public Document ServerAnswerSuccessRegistration (String sesionID) {
        XMLRequest msg = new XMLRequest (Tags.SUCCESS.getStr (), null, null, null);
        msg.appendMessage (Tags.SESSION.getStr (), Tags.SUCCESS.getStr (), null, null, sesionID);
        return msg.getDoc ();
    }

    public Document ServerAnswerErrorRegistration (String textReason) {
        XMLRequest msg = new XMLRequest (Tags.ERROR.getStr (), null, null, null);
        msg.appendMessage (Tags.MESSAGE.getStr (), Tags.ERROR.getStr (), null, null, textReason);
        return msg.getDoc ();
    }

    //2Get list of users
    public Document ClientRequestGetListOfUsers (String sessionID) {
        XMLRequest msg = new XMLRequest (Tags.COMMAND.getStr (), Tags.NAME.getStr (), Tags.LIST.getStr (), null);
        msg.appendMessage (Tags.SESSION.getStr (), Tags.COMMAND.getStr (), null, null, sessionID);
        return msg.getDoc ();
    }

    public Document ServerAnswerErrorGetListOfUsers (String textReason) {
        return ServerAnswerErrorRegistration(textReason);
    }

    public Document ServerAnswerSuccessGetListOfUsers (List<ServerObserver> users) {
        XMLRequest msg = new XMLRequest (Tags.SUCCESS.getStr (),null, null, null);
        msg.appendMessage (Tags.LISTUSERS.getStr (),Tags.SUCCESS.getStr (), null, null, null);
        int amountOfUsers = users.size ();
        for (ServerObserver user : users) {
            msg.appendMessage (Tags.USER.getStr (), Tags.LISTUSERS.getStr (), null, null, null);
            msg.appendMessage (Tags.NAME.getStr (), Tags.USER.getStr (), null, null, user.getName ());
            msg.appendMessage (Tags.TYPE.getStr (), Tags.USER.getStr (), null, null, user.getSessionId ());
        }
        return msg.getDoc ();
    }

    //3message from client to server
    public Document ClientRequestMessageToServer ( String message, String sessionID) {
        XMLRequest msg = new XMLRequest (Tags.COMMAND.getStr (), Tags.NAME.getStr (), Tags.MESSAGE.getStr (), null);
        msg.appendMessage (Tags.MESSAGE.getStr (), Tags.COMMAND.getStr (), null, null, message);
        msg.appendMessage (Tags.SESSION.getStr (), Tags.COMMAND.getStr (), null, null, sessionID);
        return msg.getDoc ();
    }

    public Document ServerAnswerErrorMessageToServer (String textReason) {
        return ServerAnswerErrorRegistration(textReason);
    }

    public Document ServerAnswerSuccessMessageToServer () {
        XMLRequest msg = new XMLRequest (Tags.SUCCESS.getStr (), null,null,null);
        return msg.getDoc ();
    }

    //4message from server to client
    public Document ServerRequestMessageToClient(String message, String chatNameFrom) {
        XMLRequest msg = new XMLRequest (Tags.EVENT.getStr (), Tags.NAME.getStr (),Tags.MESSAGE.getStr (),null);
        msg.appendMessage (Tags.MESSAGE.getStr (),Tags.EVENT.getStr (), null,null, message);
        msg.appendMessage (Tags.NAME.getStr (), Tags.EVENT.getStr (), null, null, chatNameFrom );
        return msg.getDoc ();
    }

    //5 logout
    public Document ClientRequestLogout(String sessionID) {
        XMLRequest msg = new XMLRequest (Tags.COMMAND.getStr (), Tags.NAME.getStr (), Tags.LOGOUT.getStr (), null);
        msg.appendMessage (Tags.SESSION.getStr (),Tags.COMMAND.getStr (), null, null, sessionID);
        return msg.getDoc ();
    }

    public Document ServerAnswerErrorLogout (String textReason) {
        return ServerAnswerErrorRegistration(textReason);
    }

    public Document ServerAnswerSuccessLogout () {
        return ServerAnswerSuccessMessageToServer ();
    }

    //6 new User

    public Document ServerRequestNewUser(String username) {
        XMLRequest msg = new XMLRequest (Tags.EVENT.getStr (), Tags.NAME.getStr (),Tags.USERLOGIN.getStr (),null);
        msg.appendMessage (Tags.NAME.getStr (), Tags.EVENT.getStr (), null, null, username);
        return msg.getDoc ();
    }

    //7 disconnect

    public Document ServerRequestUserDisconnect(String username) {
        XMLRequest msg = new XMLRequest (Tags.EVENT.getStr (), Tags.NAME.getStr (),Tags.USERLOGOUT.getStr (),null);
        msg.appendMessage (Tags.NAME.getStr (), Tags.EVENT.getStr (), null, null, username);
        return msg.getDoc ();
    }







}

