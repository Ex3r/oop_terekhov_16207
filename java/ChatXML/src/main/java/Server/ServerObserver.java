package Server;

import org.w3c.dom.Document;

public interface ServerObserver  {
    String getName ();
    String getSessionId();
    String getChatClientName();
    void sendMsgToSelf(Document msg);


}
