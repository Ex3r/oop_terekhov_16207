package Server;

import org.w3c.dom.Document;
import java.io.*;
import java.net.Socket;

public class User  implements ServerObserver {
    private int id;
    private String sessionId;
    private String name;
    private String chatClientName;
    private Socket socket;
    private InputStream in;
    private OutputStream out;
    private Sender sender;

    public User(Socket socket) {
        this.socket = socket;
        try {
            in = socket.getInputStream ();
            out = socket.getOutputStream ();
        } catch (IOException e) {
            e.printStackTrace ();
        }
    }

    public InputStream getInput() {
        return in;
    }

    public OutputStream getOutput() {
        return out;
    }


    public void setName(String userName) {
        this.name = userName;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public void setChatClientName(String chatClientName) {
        this.chatClientName = chatClientName;
    }

    @Override
    public String  getName() {
        return this.name;
    }

    @Override
    public String getSessionId() {
        return this.sessionId;
    }

    @Override
    public String getChatClientName() {
        return chatClientName;
    }

    @Override
    public void sendMsgToSelf(Document msg) {
        sender.sendMessage (msg, (DataOutputStream) out);
    }
}
