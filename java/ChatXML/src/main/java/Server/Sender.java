package Server;
import General.AbstractSender;
import General.ConverterDocToBytes;
import General.ReadyXMLMessages;
import org.w3c.dom.Document;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Sender implements AbstractSender {
    public void sendMessage(Document message, DataOutputStream outputStream) {
        ByteArrayOutputStream byteMessage = ConverterDocToBytes.convertMsgToOut (message);
        int messageSize = byteMessage.size ();
        try {
            outputStream.writeInt (messageSize);
            outputStream.write (byteMessage.toByteArray ());

        } catch (IOException e) {
            e.printStackTrace ();
        }
    }


}
