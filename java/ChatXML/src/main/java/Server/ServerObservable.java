package Server;

import org.w3c.dom.Document;


public interface ServerObservable  {
    void addObserver(ServerObserver o);
    void removeObserver (ServerObserver o);
    void notifyObserver(Document msg, ServerObserver curObserver);
}
