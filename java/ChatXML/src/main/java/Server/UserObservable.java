package Server;

import org.w3c.dom.Document;
import java.util.ArrayList;
import java.util.List;

public class UserObservable implements ServerObservable{
    private List<ServerObserver> activeUsers;

    public UserObservable() {
        this.activeUsers = new ArrayList<> ();
    }

    @Override
    public void addObserver(ServerObserver o) {
        activeUsers.add (o);
    }

    @Override
    public void removeObserver(ServerObserver o) {
        activeUsers.remove (o);
    }

    @Override
    public void notifyObserver(Document msg, ServerObserver observer) {
        activeUsers.get (activeUsers.indexOf (observer)).sendMsgToSelf (msg);
    }

    public void notifyAllObservers(Document msg) {
        activeUsers.forEach(user -> user.sendMsgToSelf (msg));
    }

    public void notifyAllExceptCurrent(Document msg, ServerObserver observer) {
        activeUsers.stream ().filter (user -> !user.getName ().equals (observer.getName ())).forEach (user -> user.sendMsgToSelf (msg));
    }

    public void notifyCurrentObserver(Document msg, ServerObserver observer) {
        notifyObserver (msg,observer);
    }

    public List<ServerObserver> getActiveUsers() {
        return activeUsers;
    }
}
