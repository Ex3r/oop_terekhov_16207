package Server;

import java.net.Socket;
import java.util.List;

public class ThreadPool {
    private List<Thread> connections;


    public void  addConnection(Socket userSocket, UserObservable monitor) {
        Connection newConnection = new Connection (userSocket, monitor);
        Thread newThread = new Thread (newConnection);
        newThread.start ();
        connections.add(newThread);
    }
}
