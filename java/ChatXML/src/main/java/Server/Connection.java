package Server;

import ChatException.Server.IncorrectData;
import Commands.OnServer.Commands;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Connection implements Runnable {
    private Socket socket;
    private DataInputStream input;
    private DataOutputStream output;
    private UserObservable userObservable;



    public Connection(Socket socket, UserObservable userObs) {
        try {
            userObservable = userObs;
            input = new DataInputStream (socket.getInputStream ());
            output = new DataOutputStream (socket.getOutputStream ());
        } catch (IOException e) {
            e.printStackTrace ();
        }

    }


    //как проверять , если умер поток , то удалить его из тредпула?????
    @Override
    public void run() {
        while(Thread.currentThread ().isAlive () && !socket.isClosed ()) {
            ServerMessageHandler handler = new ServerMessageHandler (new Commands ());
            try {
                handler.handleMsg (socket, input, output, userObservable);
            } catch (IncorrectData incorrectData) {
                incorrectData.printStackTrace ();
            }

        }
    }
}
