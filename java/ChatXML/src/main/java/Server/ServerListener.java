package Server;

import General.AbstractListener;
import java.io.*;

public class ServerListener implements AbstractListener{
    public ServerListener() {
    }

    public ByteArrayInputStream getMessage(DataInputStream input)  {
        int messageSize;
        byte[] byteMessage = new byte[0];
        try {
            messageSize = input.readInt ();
            byteMessage = new byte[messageSize];
            input.read (byteMessage, 0 , messageSize);

        } catch (IOException e) {
            e.printStackTrace ();
        }
        return new ByteArrayInputStream (byteMessage);
    }

  /*  public void handleMessage(ByteArrayInputStream byteMessage, OutputStream output) {
        String name, type,session, message;
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder ().parse (byteMessage);
            doc.getDocumentElement ().normalize ();
            Node root = doc.getDocumentElement ();
            if (root.getNodeType() != Node.ELEMENT_NODE) {
                return;
            }

            Element element = (Element)root;

            switch (element.getAttribute (commandName)) {
                case ("login") : {
                    name = element.getElementsByTagName ("name").item (0).getTextContent ();
                    type = element.getElementsByTagName ("type").item (0).getTextContent ();
                    break;
                }
                case ("list") : {
                    session =  element.getElementsByTagName ("session").item (0).getTextContent ();
                    break;
                }

                case("message") : {
                    message = element.getElementsByTagName ("message").item (0).getTextContent ();
                    session = element.getElementsByTagName ("session").item (0).getTextContent ();
                    Document toSend = msgCreator.ServerAnswerSuccessMessageToServer ();
                    sender.sendMessage (toSend, output);
                }
                case ("logout") : {
                   session =  element.getElementsByTagName ("session").item (0).getTextContent ();
                }
                default: {
                    throw new RuntimeException ("Unknown tag");
                }

            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace ();
        }
    }*/

}

