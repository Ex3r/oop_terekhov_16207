package Server;

import ChatException.Server.IncorrectData;
import ChatException.UnknownCommand;
import Commands.OnServer.*;
import General.AbstractMessageHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.Socket;


public class ServerMessageHandler implements AbstractMessageHandler {
    private ServerListener listener;
    private  Commands commands;

    public ServerMessageHandler(Commands commandsOnServer) {
        this.listener = new ServerListener ();
        this.commands = commandsOnServer;
    }

    public void handleMsg(Socket socket, DataInputStream input, DataOutputStream output, UserObservable userObs) throws IncorrectData {
        try {
            parse (listener.getMessage (input), socket, userObs);
        } catch (UnknownCommand unknownCommand) {
            unknownCommand.printStackTrace ();
        }

    }

    private void parse(ByteArrayInputStream byteMessage, Socket socket, UserObservable userObs) throws UnknownCommand, IncorrectData {

        Document doc;
        try {
            doc = DocumentBuilderFactory.newInstance ().newDocumentBuilder ().parse (byteMessage);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            //TODO может быть обработать иначе
            final String errMsg = "Ошибка при парсинге сообщения в самом начале:(";
            throw new UnknownCommand (errMsg);
        }
        if (doc != null) {
            doc.getDocumentElement ().normalize ();
        } else
        {
            final String errMsg = "Ошибка при парсинге сообщения в самом начале:(";
            throw new UnknownCommand (errMsg);
        }
        Node root = doc.getDocumentElement ();

        if (root.getNodeType () != Node.ELEMENT_NODE) {
            return;
        }

        Element element = (Element) root;
        String command = element.getAttribute ("name");
        CommandOnServer tmpCommand = commands.getCommand (command);
        if (tmpCommand == null) {
            throw new UnknownCommand (" on server while parsing");
        }
        tmpCommand.executeCommand (userObs, element, socket);
    }
}
