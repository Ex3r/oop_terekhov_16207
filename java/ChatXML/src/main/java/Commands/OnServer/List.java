package Commands.OnServer;

import Server.Sender;
import Server.ServerObserver;
import Server.UserObservable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;


public class List extends CommandOnServer{
    private boolean validate(String sessionID ) {
        return !"".equals (sessionID);
    }
    @Override
    public void executeCommand(UserObservable monitor, Element element, Socket socket) {
        java.util.List<ServerObserver> users = monitor.getActiveUsers ();
        String sessionID =  element.getElementsByTagName ("session").item (0).getTextContent ();
        if (!validate (sessionID)) {
            final String errMsg = "Сессия не должна быть пустым значением";
            Document msg = msgCreator.ServerAnswerErrorGetListOfUsers (errMsg);
            Sender sender = new Sender ();
            try {
                sender.sendMessage (msg, (DataOutputStream) socket.getOutputStream ());
            } catch (IOException e) {
                e.printStackTrace ();
            }
        }
        //поиск юзера с необходимой сессией
        ServerObserver user = users.stream ().filter (u ->u.getSessionId ().equals (sessionID)).findFirst ().orElse (null);
        if (user!=null) {
            //если найден, то отправляем ему успешный ответ от сервера
            Document msg = msgCreator.ServerAnswerSuccessGetListOfUsers (users);
            monitor.notifyCurrentObserver (msg, user);
        }
        else{
            final String errMsg = "Пользователя с такой сессией не существует";
            Document msg = msgCreator.ServerAnswerErrorGetListOfUsers (errMsg);
            Sender sender = new Sender ();
            try {
                sender.sendMessage (msg, (DataOutputStream) socket.getOutputStream ());
            } catch (IOException e) {
                e.printStackTrace ();
            }
        }
    }
}
