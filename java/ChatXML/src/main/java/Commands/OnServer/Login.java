package Commands.OnServer;
import ChatException.Server.IncorrectData;
import General.ReadyXMLMessages;
import Server.ServerObserver;
import Server.User;
import Server.UserObservable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.net.Socket;
import java.util.List;

public class Login extends CommandOnServer {
    public Login() {
       msgCreator = new ReadyXMLMessages ();

    }

    private boolean validate(String name, String chatClientName ) {
        return !"".equals (name) && !"".equals (chatClientName);
    }

    private String generateSessionId (String name) {
        return name;
    }

    private Document replyToClient(boolean success, String text) {
        Document message;
        if (success) {
            message = msgCreator.ServerAnswerSuccessRegistration (text);
        } else {
            message = msgCreator.ServerAnswerErrorRegistration (text);
        }
        return message;
    }

    private Document replyToOthers(String userName) {
        return msgCreator.ServerRequestNewUser (userName);
    }

    @Override
    public void executeCommand(UserObservable monitor, Element element, Socket socket) throws IncorrectData {
        List<ServerObserver> users = monitor.getActiveUsers ();
        String name = element.getElementsByTagName ("name").item (0).getTextContent ();
        String chatClientName = element.getElementsByTagName ("type").item (0).getTextContent ();
        if (!validate(name, chatClientName)) {
            final String errMsg ="Incorrect data for login";
            throw new IncorrectData (errMsg);
        }
        User newUser = new User (socket);
        newUser.setName (name);
        newUser.setChatClientName (chatClientName);

        //возможно нужно вынести этот кусок в функцию
        //TODO оттестировать корректо ли работает этот метод
        String sessionID;
        if (users.stream ().noneMatch (u -> u.getName ().equals (name))) {
            sessionID = generateSessionId (name);
            newUser.setSessionId (sessionID);
            monitor.addObserver (newUser);
            Document messageToCurrentClient  = replyToClient (true, sessionID);
            monitor.notifyCurrentObserver (messageToCurrentClient, newUser);

            Document messageToOther = replyToOthers (name);
            monitor.notifyAllExceptCurrent (messageToOther, newUser);

        } else {
            final String errMsg = "User names must be unique!";
            newUser.sendMsgToSelf ( msgCreator.ServerAnswerErrorRegistration (errMsg));
        }
    }
}
