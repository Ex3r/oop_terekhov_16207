package Commands.OnServer;

import Server.User;

import java.util.HashMap;
import java.util.Map;

public class Commands {
    private Map<String, CommandOnServer> commands;

    public Commands() {
        this.commands = new HashMap<> () {
            {
                put ("login", new Login ());
                put ("list", new List ());
                put ("message", new Message ());
                put ("logout", new Logout ());
            }
        };
    }

    public void addNewCommand(String key, CommandOnServer command) {
        if (!commands.containsKey (key)) {
            commands.put(key, command);
        }
    }

    public CommandOnServer getCommand(String key) {
        return commands.get (key);
    }
}
