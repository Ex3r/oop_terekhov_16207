package Commands.OnServer;
import Server.Sender;
import Server.ServerObserver;
import Server.UserObservable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class Message extends CommandOnServer{

    private boolean validate(String sessionID ) {
        return !"".equals (sessionID);
    }

    @Override
    public void executeCommand(UserObservable monitor, Element element, Socket socket) {
        List<ServerObserver> users = monitor.getActiveUsers ();
        String message = element.getElementsByTagName ("message").item (0).getTextContent ();
        String sessionID = element.getElementsByTagName ("session").item (0).getTextContent ();

        if (!validate (sessionID)) {
            final String errMsg = "Сессия не должна быть пустым значением!!";
            Document msg = msgCreator.ServerAnswerErrorGetListOfUsers (errMsg);
            Sender sender = new Sender ();
            try {
                sender.sendMessage (msg, (DataOutputStream) socket.getOutputStream ());
            } catch (IOException e) {
                e.printStackTrace ();
            }
        }

        ServerObserver user = users.stream ().filter (u ->u.getSessionId ().equals (sessionID)).findFirst ().orElse (null);
        if (user!=null) {
            monitor.notifyCurrentObserver (msgCreator.ServerAnswerSuccessMessageToServer (),user);
            monitor.notifyAllExceptCurrent (msgCreator.ServerRequestMessageToClient (message,user.getChatClientName ()),user);
        }
        else{
            final String errMsg = "Пользователя с такой сессией не существует, ошибка при отправке сообщения";
            Document msg = msgCreator.ServerAnswerErrorMessageToServer (errMsg);
            Sender sender = new Sender ();
            try {
                sender.sendMessage (msg, (DataOutputStream) socket.getOutputStream ());
            } catch (IOException e) {
                //TODO????
                e.printStackTrace ();
            }
        }

    }
}
