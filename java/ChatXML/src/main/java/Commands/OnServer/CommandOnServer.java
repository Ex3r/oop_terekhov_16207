package Commands.OnServer;

import ChatException.Server.IncorrectData;
import General.ReadyXMLMessages;
import Server.User;
import Server.UserObservable;
import org.w3c.dom.Element;

import java.net.Socket;


public abstract class CommandOnServer {
    protected ReadyXMLMessages msgCreator;
    public abstract void executeCommand(UserObservable monitor, Element element, Socket socket) throws IncorrectData;

}

