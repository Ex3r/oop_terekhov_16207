import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.*;

public class Main{
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(8080)) {
            while (true) {
                try (Socket clientSocket = serverSocket.accept();
                     DataInputStream in = new DataInputStream(clientSocket.getInputStream());
                     DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream())) {
                    final String nick = in.readUTF();
                    final String message = in.readUTF();
                    System.out.println(String.format("%s: %s", nick, message));
                    out.writeUTF(String.format("Hello, %s!", nick));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
