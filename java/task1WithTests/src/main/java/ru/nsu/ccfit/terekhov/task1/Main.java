package ru.nsu.ccfit.terekhov.task1;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        if ((args == null || args.length == 0)) {
            System.out.println ("Proper Usage is: java program filename");
            return;
        }

        final String filename = args[0];
        Reader reader = new Reader (filename);
        reader.read ();
        Set<Container> sortedSet = Writer.sort (reader.getStorage ());
        Writer.writeCSV (sortedSet, filename);
    }
}




