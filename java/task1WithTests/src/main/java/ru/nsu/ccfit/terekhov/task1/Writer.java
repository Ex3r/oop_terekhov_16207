package ru.nsu.ccfit.terekhov.task1;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

public class Writer {
   private static  final String standartDelimeter = ",";
   private static String createNewFilename(String oldFilename) {
        return oldFilename.replaceFirst("[.][^.]+$", "")+".csv";
   }

    private static void printHeader(FileWriter writer) {
        try {
            writer.write(String.format ("%s%s%s%s%s%s\n","Word",standartDelimeter,"Frequency",standartDelimeter,"Frequency(%)",standartDelimeter));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeCSV(Set<Container> storage, String filename) {
        FileWriter fileWriter = null;
        String filenameWithCsvExt = createNewFilename(filename);

        try(FileWriter writer = new FileWriter(filenameWithCsvExt))
        {
            printHeader (writer);
            for(Container object :storage) {
                writer.write (String.format ("%s%s%d%s%s\n", object.getWord (), standartDelimeter, object.getAmount (), standartDelimeter, object.getFrequency ()));
            }

        } catch (IOException e) {
            e.printStackTrace ();
        }

    }

    public static Set<Container> sort(Set<Container> storage) {//ISSUE в задании требовалось реализовать интерфейс Comparable для объекта контейнера
        storage = storage.stream()
                    .sorted(Comparator
                    .comparing(Container::getFrequency)
                    .reversed())
                    .collect(Collectors.toSet());

        return storage;
    }
}

