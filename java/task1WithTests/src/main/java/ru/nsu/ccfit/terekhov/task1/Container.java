package ru.nsu.ccfit.terekhov.task1;

public class Container {
    private String word;
    private Integer amount;
    private Integer frequency;

    Container(String word) {
        this.word = word;
        this.amount = 1;
        this.frequency = 0;
    }

    public String getWord() {
        return word;
    }

    public Integer getAmount() {
        return amount;
    }

    public double getFrequency() {
        return frequency;
    }

    public void increment() {
        this.amount++;
    }

    public void updateFrequency(double amountOfAllWords) throws ArithmeticException {
        frequency = (int) ((amount / amountOfAllWords) * 100);
    }
}
