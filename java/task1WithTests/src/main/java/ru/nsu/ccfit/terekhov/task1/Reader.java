package ru.nsu.ccfit.terekhov.task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Reader {
    
    private  String filename;
    private Set<Container> storage;
    private int amountOfAllWords;
    
    public Reader(String filename) {
       this.filename = filename;
       amountOfAllWords = 0;
       storage = new HashSet<>();
    }

    public int read() {
        File inputFile = new File(filename);
        try(Scanner input = new Scanner(inputFile).useDelimiter("^\\W")) {
            while(input.hasNext()) {
                String word = input.nextLine();
                if (word.isEmpty ()) {
                    continue;
                }
                addToStorage(word, storage);
                amountOfAllWords++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace ();
        }

        try {
                computeFrequencyForAllElements();
            } catch (ArithmeticException ex) {
                 ex.printStackTrace();
            }

        return 0;
    }

    private void addToStorage(String curString, Set<Container> storage) {
        if (!isElementInSet(curString)) {
            storage.add(new Container(curString));
            return;
        }

        for (Container temp : storage) {
            if (temp.getWord ().equals (curString)) {
                temp.increment ();
            }
        }
    }

    private void computeFrequencyForAllElements() throws ArithmeticException {
        storage.forEach(element -> element.updateFrequency(amountOfAllWords));
    }

    private boolean isElementInSet(String curString) {
        boolean contains = false;
        for (Container element : storage) {//ISSUE о ужас, ну есть метод contains, но вместо сета все равно нужно использовать hashMap, т.к. вставка с пробеганием по всему сету для поиска элемента вообще не радует.
        //когда будешь делать через map, обрати внимание на метод merge
            //ISSUE ISSUE contains вообще никаким образом не работал, я переопределял equals и hashCode , всё равно ничего не работало, просто убил часа 3 на это
           if (element.getWord().equals(curString)) {
               contains = true;
           }
        }
        return contains;
    }

    public Set<Container> getStorage() {
        return storage;
    }
    
}
