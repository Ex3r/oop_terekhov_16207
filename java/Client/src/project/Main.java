package project;

import java.net.InetAddress;

import java.net.Socket;
import java.util.Scanner;
import java.io.*;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner (System.in);
        String mes;
        String nick = "bla-bla-bla";

        for (int i = 0; i < 5; ++i) {
            try (Socket socket = new Socket (InetAddress.getByName ("192.168.1.40"), 8080);
                 DataInputStream in = new DataInputStream (socket.getInputStream ());
                 DataOutputStream out = new DataOutputStream (socket.getOutputStream ())) {

                mes = scan.nextLine ();
                out.writeUTF (nick);
                out.writeUTF (mes);
                final String response = in.readUTF ();
                System.out.println ("server response: " + response);
            } catch (IOException e) {
                e.printStackTrace ();
            }
        }
    }
}




