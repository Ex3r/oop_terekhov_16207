package ru.nsu.ccfit.terekhov.task1WithStreamAPI;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

class Implementation {
    private static final  String standartDelimeter = ",";
    static Map<String, Long> read(String filename) {
        Map<String, Long> storage = null;
        try {
            Stream<String> streamFromFiles = Files.lines(Paths.get(filename));
            storage = streamFromFiles.map(w -> w.split("^\\W"))
                    .flatMap(Arrays::stream)
                    .filter(word -> !word.isEmpty())
                    .collect(groupingBy(identity(), counting()));
                    //ISSUE Вся реализация лабораторной через Stream API должна занимать примерно 20 строк, можно в одном stream подсчитать общую сумму через метод peek и внешнего счетчика, затем отсортировать и вывести через forEach
        } catch (IOException e) {
            e.printStackTrace();
        }
        return storage;
    }

    static LinkedHashMap<String, Long> sortByValue(Map<String, Long> storage) {
        return storage.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));
    }

    public static String getFilenameWithCSVExt (String oldFilename) {
        return oldFilename.replaceFirst("[.][^.]+$", "")+".csv";
    }

    public static void printHeader(FileWriter writer) {
        try {
            writer.write(String.format("%s%s%s%s%s%s\n","Word",standartDelimeter,"Frequency",standartDelimeter,"Frequency(%)",standartDelimeter));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static Long countWords (LinkedHashMap<String, Long> sortedStorage) {
        return sortedStorage.entrySet().stream()
                .mapToLong(Map.Entry::getValue)
                .sum();
    }
    static void writeToFile(LinkedHashMap<String, Long> sortedStorage, String filename) {
        Long amountWords = countWords (sortedStorage);
        String filenameWithCSVExt = getFilenameWithCSVExt (filename);
        try(FileWriter writer = new FileWriter(filenameWithCSVExt)) {
            printHeader(writer);
            sortedStorage.forEach((key, value) -> {
                try {
                    writer.write(String.format ("%s%s%d%s%d\n", key, standartDelimeter, value, standartDelimeter,
                            (int)(value *100 / amountWords)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
