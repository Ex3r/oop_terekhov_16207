package ru.nsu.ccfit.terekhov.task1WithStreamAPI;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static ru.nsu.ccfit.terekhov.task1WithStreamAPI.Implementation.getFilenameWithCSVExt;
import static ru.nsu.ccfit.terekhov.task1WithStreamAPI.Implementation.printHeader;

public class Main {
    public static void main(String[] args) {
        if ((args == null || args.length == 0)) {
            System.out.println ("Proper Usage is: java program filename");
            System.exit (0);
        }

        final String filename = args[0];
        final String filenameWithCSVExt = getFilenameWithCSVExt (filename);
        final  String standartDelimeter = ",";
        AtomicInteger amount = new AtomicInteger();

        try(FileWriter writer = new FileWriter(filenameWithCSVExt))
        {
            printHeader (writer);
        } catch (IOException e) {
            e.printStackTrace ();
        }
        try {
            Files.lines (Paths.get (filename))
                    .map (w -> w.split ("^\\W"))
                    .flatMap (Arrays::stream)
                    .filter (word -> !word.isEmpty ())
                    .peek (element -> amount.getAndIncrement ())
                    .collect (groupingBy (identity (), counting ()))
                    .entrySet ().stream ()
                    .sorted (Map.Entry.comparingByValue (Comparator.reverseOrder ()))
                    .forEach (element ->
                    {
                        try(FileWriter writer = new FileWriter(filenameWithCSVExt, true)) //ISSUE файл открывается и закрывается многократно для вывода каждого элемента. Вынеси в try выше это
                        {
                            writer.write (String.format ("%s%s%d%s%d\n", element.getKey (), standartDelimeter, element.getValue (), standartDelimeter,
                                    (int)(element.getValue() *100 / amount.get() )));
                        } catch (IOException e) {
                            e.printStackTrace ();
                        }

                    });
        } catch (IOException e) {
            e.printStackTrace ();
        }
    }

}
/*Map<String, Long> storage = read(filename);
LinkedHashMap<String, Long> sortedStorage = sortByValue(storage);
writeToFile(sortedStorage, filename);*/
