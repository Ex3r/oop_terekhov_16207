
#ifndef WRITETOFILE_H
#define WRITETOFILE_H
#include "header.h"
class WriteToFile
{
public:
	static void writeToFile(std::list<std::string>& data, std::string filename, std::string& output);
};
#endif // WRITETOFILE_H
