#ifndef SORTWORKER_H
#define SORTWORKER_H
#include "header.h"
#include "worker.h"
class SortWorker: public Worker
{
public:
	explicit SortWorker();
	void process(std::list<std::string>& data, std::string& output) override;
	~SortWorker() {};
};
#endif // SORTWORKER_H
