#ifndef READWORKER_H
#define READWORKER_H
#include "header.h"
#include "worker.h"


class ReadWorker: public Worker
{
	std::string filename;
public:
	explicit ReadWorker(std::string filename);
	void process(std::list<std::string>& rawFile, std::string& output) override;
	~ReadWorker(){};
};
#endif // READWORKER_H
