#ifndef DUMPWORKER_H
#define DUMPWORKER_H
#include "header.h"
#include "worker.h"
class DumpWorker: public Worker
{
	std::string filename;
public:
	void process(std::list<std::string>& data, std::string& output) override;
	DumpWorker(std::string filename);
	~DumpWorker(){};
};
#endif // DUMPWORKER_H
