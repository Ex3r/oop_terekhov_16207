#include "replaceWorker.h"

ReplaceWorker::ReplaceWorker(const std::string oldWord, const std::string newWord)
	:oldWord(oldWord), newWord(newWord) {};


void ReplaceAll(std::string& str, const std::string& from, const std::string& to) {
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) 
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
	}
}

void ReplaceWorker::process(std::list<std::string>& data, std::string& output)
{

	for (std::list<std::string>::iterator iter = data.begin(); iter != data.end(); ++iter)
	{
		ReplaceAll(*iter, oldWord, newWord);
	}
}


