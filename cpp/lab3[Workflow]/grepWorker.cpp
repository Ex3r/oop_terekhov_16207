#include "grepWorker.h"
#include <algorithm>

GrepWorker::GrepWorker(std::string wordArgument)
{
	word = wordArgument;
}
inline size_t found(std::list<std::string>::iterator& iter, std::string word)
{
	size_t index = -1;
	index = (*iter).find(word);
	return index;
}
void GrepWorker::process(std::list<std::string>& data, std::string& output)
{
	for(std::list<std::string>::iterator iter = data.begin(); iter!=data.end();)
	{
		if(found(iter,word) == -1)
		{
			iter = data.erase(iter);
		}
		else
		{
			++iter;
		}
	}
}