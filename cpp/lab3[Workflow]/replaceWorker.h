#ifndef REPLACEWORKER_H
#define REPLACEWORKER_H
#include "header.h"
#include "worker.h"
class ReplaceWorker:public Worker
{
	std::string oldWord;
	std::string newWord;
public:
	void process(std::list<std::string>& data, std::string& output) override;
	ReplaceWorker(const std::string oldWord, const std::string newWord);
	~ReplaceWorker(){};

};
#endif // REPLACEWORKER_H
