#include "parser.h"
#include "readfileWorker.h"
#include "grepWorker.h"
#include "sortWorker.h"
#include "replaceWorker.h"
#include "dumpWorker.h"
#include "ParserUtils.h"

Parser::Parser(std::string nameOfWorkFile)
{   

	try
	{
		stringNumberForError = 0;
		std::string output;
		ReadWorker reader(nameOfWorkFile);
		reader.process(fileForParse, output);

	}
	catch(std::exception& e)
	{
		throw e;
	}
	
}

void checkLabel(bool isClosingLabel, std::string& curStr, size_t& numberOfString)
{
	const std::string tempStr = ParserUtils::trim(curStr);
	if (isClosingLabel == false)
	{
		if (tempStr != "desc")
		{

		    std::string errorMsg = "Error: Missed or incorrect open label";
			throw OpenLabelErorr(errorMsg,numberOfString);
		}
		++numberOfString;
		return;
	}
	if (tempStr != "csed")
	{
		std::string errorMsg = "Error: Missed or incorrect closing label";
		throw ClosingLabelErorr(errorMsg, numberOfString);
	}

	++numberOfString;
}

unsigned int readId(std::stringstream& stream, std::set<unsigned int>& ids, size_t& numberOfString)
{
	int id = -1;
	bool inLoop = false;
	while (true)
	{
		inLoop = true;
		stream >> id;
		if (!stream)
		{
			stream.clear();
			break;
		}
	}

	if (inLoop == false)
	{
		std::string errorMsg = "reading id integer erorr";
		throw IncorrectId(errorMsg,numberOfString);
	}

	if (id < 0)
	{
		std::string errorMs = "id must be positive";
		throw IncorrectId(errorMs , numberOfString);
	}
	const  bool is_in = ids.find(id) != ids.end();
	if (!is_in)
	{
		ids.insert(id);
	}
	else
	{
		std::string errorMsg = "repeated id: " + std::to_string(id);
		throw IncorrectId(errorMsg ,numberOfString);
	}
	return id;
}

void readEqualSign(std::stringstream& stream, size_t& numberOfString)
{
	std::string temp;
	if (stream.eof())
	{
		std::string errorMsg{ "equal sign" };
		throw UnexpectableEOF(errorMsg,numberOfString);
	}
	stream >> temp;
	if (temp != "=")
	{
		throw ExpectedEqualSign(temp,numberOfString);
	}
}

inline void writeTempWorker(unsigned int id, std::string blockName, std::list<std::string>& arguments,
	std::map <unsigned int, Block>&  tempListOfWorkers)
{
	static const std::map<std::string, unsigned int> block{ { "readfile", 0 },
														  { "writefile", 1 },
														  { "grep", 2 },
														  { "sort", 3 },
														  { "replace", 4 },
													      { "dump", 5 } };
	Block temp(blockName, arguments);
	tempListOfWorkers.insert({ id, temp });
}


void readCommand(std::stringstream& stream, unsigned id, std::map<unsigned, Block>& tempListOfWorkers,bool flag, size_t& numberOfString)
{
	std::list<std::string> parameters;
	std::string command{ "" };
	static const  std::map<std::string, unsigned int> possibleWorkers{ { "readfile", 1 },
																	 { "writefile", 1 },
																	 { "grep", 1 },
																	 { "sort", 0 },
																	 { "replace", 2 },
																	 { "dump", 1 } };

	if (!stream.good())
	{
		std::string errorMsg{ "name of worker" };
		throw UnexpectableEOF(errorMsg,numberOfString);
	}
	stream >> command;
	command = ParserUtils::tolower(command);
	const bool is_in = possibleWorkers.find(command) != possibleWorkers.end();

	if (!is_in)
	{
		throw UnknownWorker(command,numberOfString);
	}

	for (size_t i = 0; i != possibleWorkers.find(command)->second; i++)
	{
		std::string temp;
		if (!stream.good())
		{

			std::string errorMsg = "arguments for command: " + command;
			throw UnexpectableEOF(errorMsg,numberOfString);
		}
		stream >> temp;
		parameters.push_back(temp);
	}

	if (!stream.eof())
	{
		throw ExtraArguments(command,numberOfString);
	}

	writeTempWorker(id, command, parameters, tempListOfWorkers);
	++numberOfString;
}



void checkBlocks(std::string& curStr, std::set<unsigned int>& ids, std::map <unsigned int, Block>&  tempListOfWorkers,bool& blocksEnded,bool flag, size_t& numberOfString)
{
	std::stringstream stream(curStr);
	try
	{
		const unsigned int id = readId(stream, ids ,numberOfString);
		readEqualSign(stream, numberOfString);
		if(!flag)
		{
			readCommand(stream, id, tempListOfWorkers,!flag,numberOfString);
		}

		else
		{
			
		}

		
	}
	catch(IncorrectId& e)
	{
		try
		{
			checkLabel(true, curStr,numberOfString);
			blocksEnded = true;
		}
		catch(ClosingLabelErorr)
		{
			throw e;
		}

	}
	catch(std::exception& e)
	{
		throw e;
	}
}
void makeListsOfWorkers(Block block, std::list<Worker*>& workers)
{
	const std::string blockName = block.blockName;
	if (blockName =="readfile")//ISSUE ���� ���� ����� ������ if ��� switch � ���������� ����������, ����� �������� ����� ������� ����, ���������� ��� ������� � ��������� �� �����������
	{
		ReadWorker *readWorker = new ReadWorker(block.arguments.front());
		workers.push_back(readWorker);
	}
	if (blockName == "writefile")
	{
		WriteWorker *writeWorker = new WriteWorker(block.arguments.front());
		workers.push_back(writeWorker);
	}
	if (blockName == "grep")
	{
		GrepWorker *grepWorker = new GrepWorker(block.arguments.front());
		workers.push_back(grepWorker);
	}
	if (blockName == "sort")
	{
		SortWorker *sortWorker = new SortWorker{};
		workers.push_back(sortWorker);
	}
	if (blockName == "replace")
	{
		ReplaceWorker *replaceWorker = new ReplaceWorker(block.arguments.front(), block.arguments.back());
		workers.push_back(replaceWorker);
	}
	if (blockName == "dump")
	{
		DumpWorker *dumpWorker = new DumpWorker(block.arguments.front());
		workers.push_back(dumpWorker);
	}
}

void inline checkScheme(std::string& curStr, std::list<Worker*>& workers, std::set<unsigned int>& ids, std::map<unsigned int, Block >& tempListOfWorkers, size_t& numberOfString)
{
	std::stringstream stream(curStr);
	unsigned int tempId;
	std::string tempConnection;
	std::string garbage;
	std::set<unsigned int> tempIds;
	try
	{
		for (size_t i = 0; i != ids.size(); ++i)
		{
			if (!stream)
			{
				std::string errorMsg =  "---: scheme incorrect: not enough arguments";
				throw UnexpectableEOF(errorMsg, numberOfString);
			}

			std::stringstream::pos_type pos = stream.tellg();
			tempId = readId(stream, tempIds,numberOfString);

			if (!stream)
			{
				std::string errorMsg = "---: scheme incorrect: not enough arguments";
				throw UnexpectableEOF(errorMsg, numberOfString);
			}

			stream.clear();
			stream.seekg(pos, stream.beg);
			stream >> garbage;
			stream >> tempConnection;

			if (tempConnection != "->")
			{
				throw ExpectedConnectionSign(tempConnection, numberOfString);
			}

			const  bool is_in = ids.find(tempId) != ids.end();
			if (!is_in)
			{
				std::string errorMsg = "at scheme: id " + std::to_string(tempId) + " wasn't at file before";
				throw IncorrectId(errorMsg, numberOfString);
			}
			Block tempBlock = tempListOfWorkers.find(tempId)->second;
			if ((tempBlock.blockName == "readfile" && !workers.empty()) || ( (workers.size() == ids.size() - 1) && (tempBlock.blockName != "writefile")))
			{
				std::string errorMsg = "first block at scheme must be readfile, last - writefile";
				throw SchemeError(errorMsg , numberOfString);
			}
			makeListsOfWorkers(tempBlock, workers);
		}
		if (stream)
		{
			std::string errorMsg = "extra arguments at scheme";
			throw MustBeEOF(errorMsg, numberOfString);
		}
		++numberOfString;
	}
	catch(std::exception e)
	{
		throw e;
	}
}



std::list<Worker*> Parser::parse()
{
	std::string errorMsg;
	try
	{
		bool flag = false;
		
		ParserUtils::deleteEmtyStrings(fileForParse);
		if (fileForParse.size() == 0)
		{
			errorMsg = "File is empty";
			throw UnexpectableEOF(errorMsg,stringNumberForError);
		}

		bool blocksEnded = false;
		stringNumberForError++; 

		checkLabel(false, fileForParse.front(), stringNumberForError);
		std::list<std::string>::iterator iter = fileForParse.begin();
		++iter;
		for (iter; iter != fileForParse.end(); ++iter)
		{
			checkBlocks(*iter, ids, tempListOfWorkers, blocksEnded, flag,stringNumberForError);
			if (blocksEnded == true)
			{
				++iter;
				if(iter == fileForParse.end())
				{
					errorMsg = "scheme not found";
					throw UnexpectableEOF(errorMsg, stringNumberForError);
				}
				checkScheme(*iter,workers_,ids,tempListOfWorkers, stringNumberForError);
			}
		}
		if (blocksEnded ==false)
		{
			errorMsg = "label : label is missing";
			throw UnexpectableEOF(errorMsg , stringNumberForError);
		}
	}
	catch(OpenLabelErorr& e) //ISSUE ��������� ������ ���������� � ������, � ��� ��������� � ���������� ���������� � ����� catch(...)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (const std::out_of_range& oor)
	{
		errorMsg = "Unexpectable end of file:";
		std::cerr << errorMsg << oor.what() << std::endl;
	}
	catch (ClosingLabelErorr& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (IncorrectId& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (UnexpectableEOF& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (ExpectedEqualSign& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (UnknownWorker& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (ExtraArguments& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (ExpectedConnectionSign& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (SchemeError& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (MustBeEOF& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}
	catch (Exceptions& e)
	{
		errorMsg = e.what();
		std::cerr << errorMsg << std::endl;
		throw;
	}

	return workers_;
}

Parser::~Parser()
{
	for(std::list<Worker*>::iterator iter = workers_.begin(); iter!= workers_.end(); ++iter)
	{
		delete *iter;
	}
}

void ParametersParser::parseParameters(int argc, char* argv[])
{
	if (argc<=1)
	{
		//������ ��������
		return;
	}
	try
	{
		std::string workFile;
		std::string inputFile;
		std::string outputFile;
		std::vector<std::string> storage;

		for (size_t i = 1; i != argc; i++)
		{
			storage.push_back(argv[i]);
		}

		workFile = storage[0];
		if (ParserUtils::tolower(storage[1]) == "-i")
		{
			inputFile = storage[2];
			if (storage.size()==3)
			{
				return;
			}
			if (ParserUtils::tolower(storage[3]) =="-o")
			{
				outputFile = storage[4];
			}
			else
			{
				//������� ���������� ��������� ���� � �������
			}
			if (storage.size() !=5)
			{
				//������� ���������� ������ ��������� � �������
			}
		}
		else
		{
			if (ParserUtils::tolower(storage[1]) == "-o")
			{
				outputFile = storage[2];
			}
			else
			{
				//������� ���������� ��������� ���� � �������
			}
			if (storage.size() != 3)
			{
				//������� ���������� ������ ��������� � �������
			}
		}
	
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

}
