#include "exceptions.h"

Exceptions::Exceptions()
{
}

Exceptions::~Exceptions()
{
}

OpenFileErorr::OpenFileErorr(const std::string & err)
{
	error = err;
}

CreateFileErorr::CreateFileErorr(std::string& err)
{
	error = err;
}

OpenLabelErorr::OpenLabelErorr(std::string& err, size_t numberOfString)
{
	error = err+ "\n string number: " + std::to_string(numberOfString);
}

ClosingLabelErorr::ClosingLabelErorr(std::string& err, size_t numberOfString)
{
	error = err + "\n string number: " + std::to_string(numberOfString);;
}

IncorrectId::IncorrectId(std::string& err, size_t numberOfString)
{
	error = "Incorrect Id: " + err + "\n string number: " + std::to_string(numberOfString);
}

UnexpectableEOF::UnexpectableEOF(std::string& err, size_t numberOfString)
{
	error = "Unexpectable end of file when reading :" + err + "\n string number: " + std::to_string(numberOfString);
}

ExpectedEqualSign::ExpectedEqualSign(std::string& err, size_t numberOfString)
{
	error = "Expected equal sign, but found: " + err + "\n string number: " + std::to_string(numberOfString);
}

UnknownWorker::UnknownWorker(std::string& err, size_t numberOfString)
{
	error = "Unknown worker: " + err + "\n string number: " + std::to_string(numberOfString);
}

ExtraArguments::ExtraArguments(std::string& err, size_t numberOfString)
{
	error = "Extra arguments for worker: " + err + "\n string number: " + std::to_string(numberOfString);
}

ExpectedConnectionSign::ExpectedConnectionSign(std::string& err, size_t numberOfString)
{
	error = "Expected connection sign, but found: " + err + "\n string number: " + std::to_string(numberOfString);
}

SchemeError::SchemeError(std::string& err, size_t numberOfString)
{
	error = " Error at scheme: " + err + "\n string number: " + std::to_string(numberOfString);
}

MustBeEOF::MustBeEOF(std::string& err, size_t numberOfString)
{
	error = "After reading " + err + "must be EOF" + "\n string number: " + std::to_string(numberOfString);
}
