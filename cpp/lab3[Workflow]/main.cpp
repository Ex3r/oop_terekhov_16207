#include "header.h"
#include "readfileWorker.h"
#include "parser.h"
#include "executor.h"
#include "factory.h"

int main(int argc, char* argv[])
{
	std::string output;
	try {
		std::string workFile = argv[1];
		Factory factory;
		Parser parser = factory.createParser(workFile);
		Executor executor = factory.createExecutor(parser.parse());
	}
	catch(std::exception& e)
	{
		output = e.what();
		std::cerr << "Program has closed with exception: " + output;
	}
	

	/*std::list<std::string> rawFile;
	std::string output;
	if (argc > 1)
	{
		
	}
	ReadWorker reader("example.txt");
	reader.process(rawFile, output);
	Parser parser;
	try {
		
		Executor executor;
		executor.exec(parser.parse(rawFile, output) , output);
	}
	catch(...)
	{
		std::cout << output << std::endl;
	}
	return 0;*/
	//ParametersParser::parseParameters(argc, argv);
}
