#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H
#include "header.h"

class Exceptions: std::exception
{
	std::string emp = {};
public:
	Exceptions();
	virtual ~Exceptions();
	virtual const char *what() const
	{
		return emp.c_str();
	}
};

class OpenFileErorr: public Exceptions
{
	std::string error;
public:
	OpenFileErorr(const std::string& err);
	virtual ~OpenFileErorr() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class CreateFileErorr : public Exceptions
{
	std::string error;
public:
	CreateFileErorr(std::string& err);
	virtual ~CreateFileErorr() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class OpenLabelErorr : public Exceptions
{
	std::string error;
public:
	OpenLabelErorr(std::string& err, size_t numberOfString);
	virtual ~OpenLabelErorr() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class ClosingLabelErorr : public Exceptions
{
	std::string error;
public:
	ClosingLabelErorr(std::string& err, size_t numberOfString);
	virtual ~ClosingLabelErorr() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class IncorrectId : public Exceptions
{
	std::string error;
public:
	IncorrectId(std::string& err, size_t numberOfString);
	virtual ~IncorrectId() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class UnexpectableEOF: public Exceptions
{
	std::string error;
public:
	UnexpectableEOF(std::string& err , size_t numberOfString);
	virtual ~UnexpectableEOF() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class ExpectedEqualSign : public Exceptions
{
	std::string error;
public:
	ExpectedEqualSign(std::string& err, size_t numberOfString);
	virtual ~ExpectedEqualSign() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class UnknownWorker : public Exceptions
{
	std::string error;
public:
	UnknownWorker(std::string& err, size_t numberOfString);
	virtual ~UnknownWorker() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class ExtraArguments : public Exceptions
{
	std::string error;
public:
	ExtraArguments(std::string& err , size_t numberOfString);
	virtual ~ExtraArguments() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class ExpectedConnectionSign : public Exceptions
{
	std::string error;
public:
	ExpectedConnectionSign(std::string& err , size_t numberOfString);
	virtual ~ExpectedConnectionSign() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class SchemeError : public Exceptions
{
	std::string error;
public:
	SchemeError(std::string& err , size_t numberOfString);
	virtual ~SchemeError() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};

class MustBeEOF : public Exceptions
{
	std::string error;
public:
	MustBeEOF(std::string& err , size_t numberOfString);
	virtual ~MustBeEOF() throw() {};
	const char* what() const override
	{
		return error.c_str();
	}
};
#endif // EXCEPTIONS_H
