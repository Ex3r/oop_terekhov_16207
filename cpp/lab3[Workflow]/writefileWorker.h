#ifndef WRITEFILEWORKER_H
#define WRITEFILEWORKER_H
#include "header.h"
#include "worker.h"
class WriteWorker: public Worker
{
	std::string filename;
 public:
	explicit WriteWorker(std::string filename);
	void process(std::list<std::string>& param, std::string& output) override;
	~WriteWorker(){};
};
#endif // WRITEFILEWORKER_H
