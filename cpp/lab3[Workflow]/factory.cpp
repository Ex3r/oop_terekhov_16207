#include "factory.h"

Parser& Factory::createParser(std::string nameOfWorkFile)
{
	static Parser parser(nameOfWorkFile);
	return parser;
}

Executor& Factory::createExecutor(std::list<Worker*>& workers)
{
	static Executor executor(workers);
	return executor;
}
