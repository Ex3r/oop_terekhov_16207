#ifndef PARSERUTILS_H
#define PARSERUTILS_H
#include "header.h"
#include "parser.h"

class ParserUtils
{
public:
	static std::string trim(std::string& str);
	static std::string tolower(std::string& s);
	inline int indexOf(std::string s, std::string substring);
	static void deleteEmtyStrings(std::list<std::string>& data);
	

};
#endif // PARSERUTILS_H
