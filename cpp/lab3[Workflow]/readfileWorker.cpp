#include "readfileWorker.h"

ReadWorker::ReadWorker(std::string name)
{
	filename = name;
}

size_t getSize(std::ifstream& file)
{
	file.seekg(0, std::ios_base::end);
	const size_t fileSize = file.tellg();
	file.seekg(0, std::ios_base::beg);
	return fileSize;

}

void openFile(std::ifstream& inputFile,std::string name, std::string& output)
{
	inputFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		inputFile.open(name, std::ios::in);
	}
	catch(std::ifstream::failure)
	{
		output = "Can't open file " + name;
		throw OpenFileErorr(output);
	}

}

void reading(std::ifstream& inputFile, std::list<std::string>& file)
{
	setlocale(LC_ALL, "Rus");
	std::string tempStr;
	size_t size = getSize(inputFile);
	while(inputFile.tellg() <size)
	{
		
		std::getline(inputFile, tempStr);
		file.push_back(tempStr);
	}
}

void ReadWorker::process(std::list<std::string>& rawFile, std::string& output)
{
	std::ifstream inputFile;
	try
	{
		openFile(inputFile, filename,output);
		reading(inputFile, rawFile);
		inputFile.close();
	}
	catch(OpenFileErorr& e)
	{
		std::cerr << e.what() << std::endl;
	}
	
}
