#include "writefileWorker.h"
#include "writeToFile.h"

WriteWorker::WriteWorker(std::string name)
{
	filename = name;
}



void WriteWorker::process(std::list<std::string>& data, std::string& output)
{
	WriteToFile::writeToFile(data, filename, output);
	
}
