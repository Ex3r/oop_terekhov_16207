#ifndef EXECUTOR_H
#define EXECUTOR_H
#include "header.h"
#include "writefileWorker.h"

class BaseExecutor
{
protected:
	std::list<std::string> data;
public:
	
	virtual void exec(std::string& output) = 0;
	virtual ~BaseExecutor() {};
};

class Executor : public BaseExecutor
{
	std::list<Worker*> worksers_;
public:
	Executor(std::list<Worker*>& worksers);
	void exec(std::string& output) override;
	~Executor() {};

};

class CommandExecutor : public BaseExecutor
{
	std::list<Worker*> worksers_;
public:
	CommandExecutor(std::list<Worker*>& worksers);
	void exec(std::string& output) override;
	~CommandExecutor() {};
};
#endif // EXECUTOR_H