#include "executor.h"


Executor::Executor(std::list<Worker*>& worksers)
{
	worksers_ = worksers;
}

void Executor::exec(std::string& output)
{
	for(std::list<Worker*>::const_iterator iter = worksers_.cbegin(); iter!=worksers_.cend();++iter)
	{
		(*iter)->process(data, output);
	}
}
