#ifndef GREPWORKER_H
#define GREPWORKER_H
#include "header.h"
#include "worker.h"
class GrepWorker: public Worker
{
	std::string word;
public:
	void process(std::list<std::string>& data, std::string& output) override;
	explicit GrepWorker(std::string word);
	~GrepWorker(){};
};
#endif // GREPWORKER_H
