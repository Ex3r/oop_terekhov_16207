#ifndef PARSER_H
#define PARSER_H
#include "header.h"
#include "writefileWorker.h"

struct Block
{
	Block(std::string blockName = "", std::list<std::string> arguments = {})
		:blockName(blockName), arguments(arguments) {}
	std::string blockName;
	std::list<std::string> arguments;
};

class BaseParser
{
								
public:
	virtual std::list<Worker*> parse() = 0;
	virtual ~BaseParser() {};
};

	
class Parser: public BaseParser
{
	std::set<unsigned int> ids;
	std::list<Worker*> workers_;
	std::map<unsigned int, Block > tempListOfWorkers;
	std::list<std::string> fileForParse;
	size_t stringNumberForError;
public:
	Parser(std::string nameOfWorkFile);
	std::list<Worker*> parse() override;
	~Parser();
};

class CommandParser : public BaseParser
{
	std::list<std::string> fileForParse;
	std::set<unsigned int> ids;
	std::list<Worker*> workers_;
	std::map<unsigned int, Block > tempListOfWorkers;
public:
	CommandParser();
	std::list<Worker*> parse() override;
	~CommandParser();
};

class ParametersParser
{
public:
	//ParametersParser(){};
	static void parseParameters(int argc, char* argv[]);
	//~ParametersParser() {};
};
#endif // PARSER_H
