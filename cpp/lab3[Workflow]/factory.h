#ifndef FACTORY_H
#define FACTORY_H
#include "ParserUtils.h"
#include "executor.h"

class Factory
{

public:
	static  Parser& createParser(std::string nameOfWorkFile);
	static  CommandParser& createCommandParser(std::string nameOfWorkFile);
	static  Executor& createExecutor(std::list<Worker*>& workers);
	static  CommandExecutor& createCommandExecutor(std::list<Worker*>& workers);
    ~Factory() {};
};

#endif // FACTORY_H
