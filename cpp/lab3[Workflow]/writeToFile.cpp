#include "writeToFile.h"

bool isFileExist(std::string fileName)
{
	std::ifstream infile(fileName);
	return infile.good();
}

char askUserForRewrite(char testAnswer = 't')
{
	bool answered = false;
	char answer;
	if (testAnswer == 'n')
	{
		return 'n';
	}
	if (testAnswer == 'y')
	{
		return 'y';
	}
	while (!answered && testAnswer != 'n')
	{
		std::cin.get(answer);
		if ((answer == 'Y') || (answer == 'y') || (answer == 'N') || (answer == 'n'))
		{
			answered = true;
			break;
		}
	}
	return answer;
}
inline void write(std::ofstream& outputFile, std::list<std::string>& data)
{
	for (std::list<std::string>::const_iterator iter = data.cbegin(); iter != data.cend(); ++iter)
	{
		outputFile << (*iter) << "\n";
	}

}
void createFile(std::ofstream& outputFile, std::string filename, std::string& output)
{
	try
	{
		if (isFileExist(filename))
		{
			//��� ������
			const char answer = askUserForRewrite(output[0]);
			output = "";
			if ((answer == 'N') || (answer == 'n'))
			{
				output = "Failure to overwrite a file " + filename;
				throw CreateFileErorr(output);
			}
		}
		outputFile.open(filename, std::ios::trunc);
		output = "";
	}
	catch (std::ofstream::failure)
	{
		output = "Can't create file " + filename;
		throw CreateFileErorr(output);
	}
	catch (CreateFileErorr& e)
	{
		throw e;
	}
}
void WriteToFile::writeToFile(std::list<std::string>& data, std::string filename, std::string& output)
{
	std::ofstream outputFile;
	try
	{
		createFile(outputFile, filename, output);
		write(outputFile, data);
		outputFile.close();
	}
	catch (CreateFileErorr& e)
	{
		std::cerr << e.what() << std::endl;
	}
}
