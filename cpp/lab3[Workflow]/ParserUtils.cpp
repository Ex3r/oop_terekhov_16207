#include "ParserUtils.h"

std::string ParserUtils::trim(std::string & str)
{
	str.erase(0, str.find_first_not_of(' '));         //prefixing spaces
	str.erase(str.find_last_not_of(' ') + 1);         //surfixing spaces
	return str;
}

std::string ParserUtils::tolower(std::string& s)
{
	std::transform(s.begin(), s.end(), s.begin(),
		[](unsigned char c) { return std::tolower(c); } // correct
	);
	return s;
}

inline int indexOf(std::string s, std::string substring)
{
	int index = -1;
	index = s.find(substring);
	return index;

}

void ParserUtils::deleteEmtyStrings(std::list<std::string>& data)
{
	for (std::list<std::string>::iterator iter = data.begin(); iter != data.end();)
	{
		if ((*iter) == "")
		{
			iter = data.erase(iter);
		}
		else
		{
			++iter;
		}
	}
}

