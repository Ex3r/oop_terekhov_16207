#include <gtest/gtest.h>
#include "../lab2/controller/controller.h"
class serializerTest :public testing::Test
{

};

TEST_F(serializerTest, corectSave)
{
	std::string output;
	std::string fileName = "correctSave.txt";
	std::string correctOutput = "The game was successfully saved in file " + fileName;
	controller::Serializer saver;
	
	std::vector<std::vector<model::Cell>> world(10, std::vector<model::Cell>(10));
	for (size_t i = 0; i != world.size(); ++i)
	{
		for (size_t j = 0; j != world.size(); ++j)
		{
			if(i==j)
			{
				world[i][j] = true;
			}
		}
	}
	saver.save(world, fileName, output, 'y');
	EXPECT_EQ(correctOutput, output);
}

TEST_F(serializerTest, corectSave2)
{
	std::string output;
	std::string fileName = "correctSave2.txt";
	std::string correctOutput = "The game was successfully saved in file " + fileName;
	controller::Serializer saver;

	std::vector<std::vector<model::Cell>> world(10, std::vector<model::Cell>(10));
	for (size_t i = 0; i != world.size(); ++i)
	{
		for (size_t j = 0; j != world.size(); ++j)
		{
			if (i == j)
			{
				world[i][j] = true;
			}
		}
	}
	saver.save(world, fileName, output, 'y');
	EXPECT_EQ(correctOutput, output);
}

TEST_F(serializerTest, fileAlreadyExist_no_save)
{
	std::string output;
	std::string fileName = "correctSave2.txt";
	std::string correctOutput = "Warning: File " + fileName + " already exists. Rewrite? Y/N";
	controller::Serializer saver;
	std::vector<std::vector<model::Cell>> world(10, std::vector<model::Cell>(10));
	saver.save(world, fileName, output, 'n');
	EXPECT_EQ(correctOutput, output);
}
