#include <gtest/gtest.h>
#include "../lab2/controller/controller.h"
#include "../lab2/controller/factory.h"
#include "../lab2/controller/deserializer.h"

class strategyTest :public testing::Test
{

};
/*
 *   *
 *  * *
 */

TEST_F(strategyTest, Test_1)
{
	std::string loaderOutput;
	Factory f1;
	controller::Deserializer loader;
	controller::Executor exe;
	controller::Strategy str(f1.createWorld(), f1.createHistory());
	loader.load(str.getWorld().getField(), "test_strategy_1.txt", loaderOutput);
	EXPECT_EQ(str.getStep(), 0);
	EXPECT_EQ(str.getInfoAboutExit(), false);
	EXPECT_EQ(str.getCellInvariably(), false);
	EXPECT_EQ(str.getNumOfAliveCells(), 0);
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 3);

	str.step(1);
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 2);
	EXPECT_EQ(str.getStep(), 1);
	EXPECT_EQ(str.getCellInvariably(), false);

	str.step(1);
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 0);
	EXPECT_EQ(str.getStep(), 2);
	EXPECT_EQ(str.getCellInvariably(), false);
}

/*
 *	**
 *	**
 */
TEST_F(strategyTest, Test_2)
{
	std::string loaderOutput;
	Factory f2;
	controller::Deserializer loader;
	controller::Executor exe;
	controller::Strategy str(f2.createWorld(), f2.createHistory());
	loader.load(str.getWorld().getField(), "test_strategy_2.txt", loaderOutput);
	EXPECT_EQ(1, 1);
	EXPECT_EQ(str.getStep(), 0);
	EXPECT_EQ(str.getInfoAboutExit(), false);
	EXPECT_EQ(str.getCellInvariably(), false);
	EXPECT_EQ(str.getNumOfAliveCells(), 0);
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 4);

	str.step(1);
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 4);
	EXPECT_EQ(str.getStep(), 1);
	EXPECT_EQ(str.getCellInvariably(), false);

}

TEST_F(strategyTest, Test_back)
{
	std::string loaderOutput;
	Factory f;
	controller::Deserializer loader;
	controller::Executor exe;
	controller::Strategy str(f.createWorld(), f.createHistory());
	loader.load(str.getWorld().getField(), "test_strategy_1.txt", loaderOutput);

	EXPECT_EQ(str.getStep(), 0);
	EXPECT_EQ(str.getInfoAboutExit(), false);
	EXPECT_EQ(str.getCellInvariably(), false);
	EXPECT_EQ(str.getNumOfAliveCells(), 0);
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 3);

	str.step(1);
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 2);
	EXPECT_EQ(str.getStep(), 1);
	EXPECT_EQ(str.getCellInvariably(), false);

	str.step(1);
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 0);
	EXPECT_EQ(str.getStep(), 2);
	EXPECT_EQ(str.getCellInvariably(), false);

	str.back();
	str.countAliveCells();
	EXPECT_EQ(str.getNumOfAliveCells(), 2);
	EXPECT_EQ(str.getStep(), 1);
	EXPECT_EQ(str.getCellInvariably(), false);
}
