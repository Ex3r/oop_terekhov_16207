#include <gtest/gtest.h>
#include "../lab2/controller/controller.h"
#include "../lab2/controller/deserializer.h"

class deserializerTest :public testing::Test
{

};

TEST_F(deserializerTest, correctLoad)
{
	std::string output;
	std::string fileName = "deserializerTest_1.txt";
	std::string correctOutput;
	controller::Deserializer loader;
	std::vector<std::vector<model::Cell>> world(10, std::vector<model::Cell>(10));
	loader.load(world, fileName, output);
	EXPECT_EQ(correctOutput, output);
	
	for (size_t i = 0; i != world.size(); ++i)
	{
		for (std::vector<model::Cell>::iterator iter = (world[i]).begin(); iter != (world[i]).end(); ++iter)
		{
			bool cmp = iter->getState();
			if (i == 1 || i == 5)
			{  
				
					EXPECT_EQ(cmp, true);
			}
		}
	}
}

TEST_F(deserializerTest, thisFileIsNotExist)
{
	std::string output;
	std::string fileName = "correctSave123444312.txt";
	std::string correctOutput = "Error: Could not open  input file " + fileName;
	controller::Deserializer loader;

	std::vector<std::vector<model::Cell>> world(10, std::vector<model::Cell>(10));
	loader.load(world, fileName, output);
	EXPECT_EQ(correctOutput, output);
}

TEST_F(deserializerTest, fileIsBroken)
{
	std::string output;
	std::string fileName = "fileIsBroken.txt";
	std::string correctOutput = "Error: Incorrect CellModification in file: File size is different from standart";
	controller::Deserializer loader;

	std::vector<std::vector<model::Cell>> world(10, std::vector<model::Cell>(10));
	loader.load(world, fileName, output);
	EXPECT_EQ(correctOutput, output);
}

TEST_F(deserializerTest, dataOfFileIsNotCorrect)
{
	std::string output;
	std::string fileName = "deserializerTest_3.txt";
	std::string correctOutput = "Error: Incorrect CellModification in file: In the file must be only characters \'0\' and \'1\'";
	controller::Deserializer loader;

	std::vector<std::vector<model::Cell>> world(10, std::vector<model::Cell>(10));
	loader.load(world, fileName, output);
	EXPECT_EQ(correctOutput, output);
}