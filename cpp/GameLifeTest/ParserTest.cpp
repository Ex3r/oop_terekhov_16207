#include <gtest/gtest.h>
#include "../lab2/controller/controller.h"
#include "../lab2/controller/parser.h"

class parserTest :public testing::Test
{

};
static const std::string possibleCommands[9] = { "reset", "set", "clear", "step", "back" ,"save", "load" , "help", "exit" };
const static std::string defaultErrorValue = { "success" };
const static std::string defaultParamValue = { "???" };
TEST_F(parserTest, someCommandsAtOne)
{
	std::string command = { "save load set" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Too many different commands in one line(3)" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, correctUsageReset)
{
	std::string command = { "reset" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = defaultErrorValue;
	EXPECT_EQ(test.errorMessage, output);
	EXPECT_EQ(test.commandNum, 0);
	EXPECT_EQ(test.firstParam, defaultParamValue);
	EXPECT_EQ(test.secondParam, defaultParamValue);
	
}

TEST_F(parserTest, incorrectUsageReset1)
{
	std::string command = { "reseta" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Maybe you meant \"" + possibleCommands[0] + "\" enter \"help\" to get the detailed information about this command" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageReset2)
{
	std::string command = { "reset12" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Maybe you meant \"" + possibleCommands[0] + "\" enter \"help\" to get the detailed information about this command" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageReset3)
{
	std::string command = { "12reset" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Maybe you meant \"" + possibleCommands[0] + "\" enter \"help\" to get the detailed information about this command" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, correctUsageBack)
{
	std::string command = { "back" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = defaultErrorValue;
	EXPECT_EQ(test.errorMessage, output);
	EXPECT_EQ(test.commandNum, 4);
	EXPECT_EQ(test.firstParam, defaultParamValue);
	EXPECT_EQ(test.secondParam, defaultParamValue);

}

TEST_F(parserTest, incorrectUsageBack1)
{
	std::string command = { "backto" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Maybe you meant \"" + possibleCommands[4] + "\" enter \"help\" to get the detailed information about this command" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageBack2)
{
	std::string command = { "back12" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Maybe you meant \"" + possibleCommands[4] + "\" enter \"help\" to get the detailed information about this command" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageBack3)
{
	std::string command = { "12back" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Maybe you meant \"" + possibleCommands[4] + "\" enter \"help\" to get the detailed information about this command" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, correctUsageSetXY)
{
	std::string command = { "seta0" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = defaultErrorValue;
	EXPECT_EQ(test.errorMessage, output);
	EXPECT_EQ(test.commandNum, 1);
	EXPECT_EQ(test.firstParam, "0");
	EXPECT_EQ(test.secondParam, "0");
}

TEST_F(parserTest, incorrectUsageSetXY_first_argument)
{
	std::string command = { "setm0" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Incorrect usage \"" + possibleCommands[1] + "\" : after " + possibleCommands[1] + " must be letter from \'A\' to \'J\'" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageSetXY_second_argument1)
{
	std::string command = { "setaa" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Incorrect usage \"" + possibleCommands[1] + "\" : after " + possibleCommands[1]  + "a must be digit from \'0\' to \'9\'" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageSetXY_second_argument2)
{
	std::string command = { "seta15" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Maybe you meant \"" + possibleCommands[1] + "\" enter \"help\" to get the detailed information about this command" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, correctUsageClearXY)
{
	std::string command = { "Cleara0" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = defaultErrorValue;
	EXPECT_EQ(test.errorMessage, output);
	EXPECT_EQ(test.commandNum, 2);
	EXPECT_EQ(test.firstParam, "0");
	EXPECT_EQ(test.secondParam, "0");
}

TEST_F(parserTest, incorrectUsageClearXY_first_argument)
{
	std::string command = { "Clearm0" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Incorrect usage \"" + possibleCommands[2] + "\" : after " + possibleCommands[2] + " must be letter from \'A\' to \'J\'" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageClearXY_second_argument1)
{
	std::string command = { "Clearaa" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Incorrect usage \"" + possibleCommands[2] + "\" : after " + possibleCommands[2] + "a must be digit from \'0\' to \'9\'" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageClearXY_second_argument2)
{
	std::string command = { "Cleara15" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Maybe you meant \"" + possibleCommands[2] + "\" enter \"help\" to get the detailed information about this command" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, correctUsageStep_without_arguments)
{
	std::string command = { "step" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = defaultErrorValue;
	EXPECT_EQ(test.errorMessage, output);
	EXPECT_EQ(test.commandNum, 3);
	EXPECT_EQ(test.firstParam, "1");
}
TEST_F(parserTest, correctUsageStep_with_arguments)
{
	std::string command = { "step123" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = defaultErrorValue;
	EXPECT_EQ(test.errorMessage, output);
	EXPECT_EQ(test.commandNum, 3);
	EXPECT_EQ(test.firstParam, "123");
}

TEST_F(parserTest, incorrectUsageStep_begining_from_0)
{
	std::string command = { "step012" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Incorrect usage \"step\" : after step must be digit from \'0\' to \'9\' exept \'0\' in the first position" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageStep)
{
	std::string command = { "step123a" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Incorrect usage \"step\" : after step123 must be digit from \'0\' to \'9\' exept \'0\' in the first position" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, correctUsageLoadfile)
{
	std::string command = { "load \"test.txt\""};
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = defaultErrorValue;
	EXPECT_EQ(test.errorMessage, output);
	EXPECT_EQ(test.commandNum, 6);
	EXPECT_EQ(test.firstParam, "test.txt");
}

TEST_F(parserTest, incorrectUsageLoad_missed_quot_before_filename)
{
	std::string command = { "load test.txt\"" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Before the filename  should be opening quotation mark" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageLoad_symbols_after_closing_quot)
{
	std::string command = { "load \"test.txt\"qwerty" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "After the closing quotation there should be nothing except \"" + possibleCommands[6] + "\"" };
	EXPECT_EQ(test.errorMessage, output);
}

TEST_F(parserTest, incorrectUsageLoad_unkownCommand)
{
	std::string command = { "loadfile \"test.txt\"" };
	controller::Parser parser;
	model::CommandParameters test;
	test = parser.parseCommand(command);
	std::string output = { "Before the opening quotation mark should be nothing except \"" + possibleCommands[6] + "\"" };
	EXPECT_EQ(test.errorMessage, output);
}
