﻿#include "hashTable.h"

int main(int argc, char * argv[])
{
	
	HashTable a1,a2,a3;
	a1.insert("test1", { 20,18 });
	a1.insert("test2", { 20,18 });
	HashTable::Iterator it(a1);
	++it;
	++it;
	++it;

	for (const auto elem : a1) {
		std::cout << elem.first << " " << elem.second.age << " " << elem.second.weight << "\n";
	}
	for (const auto elem : a3) {
		std::cout << elem.first << " " << elem.second.age << " " << elem.second.weight << "\n";
	}

	return 0;
}