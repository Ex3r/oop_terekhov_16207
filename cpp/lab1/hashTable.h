﻿#ifndef HASHTABLE_H
#define HASHTABLE_H
#include <string>
#include <cstdio>	
#include <vector>
#include <list>
#include <iterator>
#include <algorithm>
#include <iostream>

struct Value {
	Value(unsigned age = 20, unsigned weight = 18)
		:age(age), weight(weight) {}
	unsigned age;
	unsigned weight;
};


class HashTable
{
	using Key = std::string;
	using Table = std::vector <std::list<std::pair<Key, Value>>*>;
	using List = std::list<std::pair<Key, Value>>;
	using HTPair = std::pair< Key, Value >;
	static int const LOAD_FACTOR = 75;
	static int const INITIAL_SIZE = 7;
	static int const MULTIPLIER = 2;
	class Iterator;

	//переменные
	size_t sizeTable_;//размер таблицы
	size_t amountOfElements;//фактическое количество элментов в таблице
	int curLoadFactor;	// % заполненности таблицы

						//методы
						//вычисление хэш-функции
	size_t hash(Key) const;

	//возвращает размер таблицы
	size_t sizeTable() const;

	//вычисление нового размера таблицы( просто число, большее предыдущего ~в 2 раза
	size_t getNextPrimeNumber(size_t);

	//пересчёт заполненности таблицы
	void updateLoadFactor();

	//увеличение хэш -таблицы
	void resize();

	/*поиск элемента по ключу
	*возвращает итератор, если найдено,
	*в противном случае table[index]->end();
	*/
	List::iterator findByKey(const Key& surname, Table&) const;

	/*
	* Удаление листов с данными
	*/
	void freeLists(Table& tableR, size_t sizeTable);

public:
	Table *table;
	HashTable();
	~HashTable();
	HashTable(const HashTable&);
	HashTable& operator=(const HashTable& b);
	
	//move construcror
	HashTable(HashTable&& old);

	//оператор присваивания перемещения для класса
	HashTable& operator=(HashTable&& old);

	/*очищает контейнер
	*возвращает вектор к исходному значению
	**/
	void clear();

	/* Вставка в контейнер. Возвращаемое значение - успешность вставки*/
	bool insert(const Key& surname, const Value&);

	// Удаляет элемент по заданному ключу. Возвращает true в случае успеха,    
	//false - если элемент с данным ключом отсутствует в хэш-таблице
	bool erase(const Key& surname);

	//возвращает текущее количество элементов в таблице
	size_t size() const;

	//Возвращает true - если таблица пустая, иначе - false
	bool empty() const;

	//Проверка наличия значения по заданному ключу.
	/*true - если есть, false - если нет	 */
	bool contains(const Key& surname) const;

	// Возвращает значение по ключу. Бросает исключение при неудаче.
	Value& at(const Key& surname);
	Value& at(const Key& surname) const;

	// Возвращает значение по ключу. Небезопасный метод.
	// В случае отсутствия ключа в контейнере следует вставить в контейнер
	// значение Value, созданное конструктором по умолчанию и вернуть ссылку на    //него. 
	Value& operator[](const Key& surname);

	// std::swap.
	void swap(HashTable& b);

	//Пара операторов для проверки на равенство хэш-таблиц. Объяснить, почему //данные методы являются друзьями, а не членами класса HashTable 
	friend bool operator==(const HashTable & a, const HashTable & b);
	friend bool operator!=(const HashTable & a, const HashTable & b);

	Iterator begin() const;
	Iterator end() const;
	

class Iterator
{
	Table::iterator iterVector;		
	std::list<std::pair<Key, Value>> ::iterator iterList;
	Table *iterTable;
	int indexVector = 0;
	int indexList = -1;
	int amountOfElements = 0;

public:

	Iterator(Table::iterator iterVector, std::list<std::pair<Key, Value>> ::iterator iterList,
		Table *iterTable = nullptr, int indexVector = 0, int indexList = -1) :
		  iterVector(iterVector)
		, iterList(iterList)
		, iterTable(iterTable)
		, indexVector(indexVector)
		, indexList(indexList){}

	Iterator(const HashTable& hashTable)
	{
		iterTable = hashTable.table;
		indexVector = 0;
		indexList = -1;
	}

	Iterator& operator++ ();

	const HTPair& operator*() const;

	Iterator& operator=(const Iterator& ci);

	bool operator==(const Iterator& ci) const;
	bool operator!=(const Iterator& ci) const;
};
};

#endif // HASHTABLE_H

