﻿#include "hashTable.h"
/**************************Вспомогательные методы*********************************/
//пересчёт степени заполненности хэш-таблицы
void HashTable::updateLoadFactor()
{
	curLoadFactor = 100 * amountOfElements / sizeTable_;
}
size_t HashTable::getNextPrimeNumber(size_t curNum)
{
	size_t nextNum = curNum * 2;
	size_t i;
	while (true)
	{
		nextNum++;

		for (i = 2; i <= nextNum / 2; i++)
		{
			if (nextNum % i == 0)
			{
				break;
			}
		}

		if (i > nextNum / 2)
		{
			break;
		}
	}

	return nextNum;
}

size_t HashTable::sizeTable() const
{
	return sizeTable_;
}

std::list<std::pair<std::basic_string<char>, Value>>::iterator HashTable::findByKey(const Key& surname, Table& table) const
{
	const size_t index = hash(surname);

	for (List::iterator iter = table[index]->begin(); iter != table[index]->end(); ++iter)
	{
		if (iter->first == surname)
		{
			return iter;
		}
	}
	return table[index]->end();
}

void HashTable::freeLists(Table& tableR, size_t sizeTable)
{
	for (auto i = 0; i != sizeTable; i++)
	{
		if (!tableR[i])
		{
			continue;
		}
		delete (tableR[i]);
		tableR[i] = nullptr;
	}
}

/**************************************************Основные методы**********************************************/
HashTable::HashTable()
{
	table = new Table(INITIAL_SIZE);
	sizeTable_ = INITIAL_SIZE;
	amountOfElements = 0;
	updateLoadFactor();
}

HashTable::~HashTable()
{
	freeLists(*table, sizeTable_);
	delete table;
	table = nullptr;
	sizeTable_ = 0;
	amountOfElements = 0;
	curLoadFactor = 0;
}

HashTable::HashTable(const HashTable& oldHashTable)
{
	const size_t tempSizeTable = oldHashTable.sizeTable();
	size_t tempAmountOfElements = 0;
	Table *tempTable = nullptr;
	try
	{
		tempTable = new Table(tempSizeTable);
		for (size_t i = 0; i != tempSizeTable; i++)
		{
			if ((*oldHashTable.table)[i])
			{
				(*tempTable)[i] = new List;
				*(*tempTable)[i] = *(*oldHashTable.table)[i];
				tempAmountOfElements++;
			}
		}
	}
	catch (std::bad_alloc& e)
	{
		freeLists(*tempTable, tempSizeTable);
		delete tempTable;
		tempTable = nullptr;
		throw e;
	}
	sizeTable_ = tempSizeTable;
	amountOfElements = tempAmountOfElements;
	table = tempTable;
	updateLoadFactor();
}

HashTable& HashTable::operator=(const HashTable &oldHashTable)
{
	if (this == &oldHashTable)
	{
		return *this;
	}

	const size_t tempSizeTable = oldHashTable.sizeTable();
	size_t tempAmountOfElements = 0;
	Table *tempTable = nullptr;
	try
	{
		tempTable = new Table(tempSizeTable);
		for (size_t i = 0; i != tempSizeTable; i++)
		{
			if ((*oldHashTable.table)[i])
			{
				(*tempTable)[i] = new List;
				*(*tempTable)[i] = *(*oldHashTable.table)[i];
				tempAmountOfElements+= ((*tempTable)[i])->size();
			}
		}
	}
	catch (std::bad_alloc& e)
	{
		freeLists(*tempTable, tempSizeTable);
		delete tempTable;
		tempTable = nullptr;
		throw e;
	}
	//удаление изначальной table
	freeLists(*table, sizeTable_);
	delete table;
	amountOfElements = tempAmountOfElements;
	sizeTable_ = tempSizeTable;
	updateLoadFactor();
	table = tempTable;
	return *this;
}

//move constructor
HashTable::HashTable(HashTable&& old)
{
	
	table = old.table;
	sizeTable_ = old.table->size();
	amountOfElements = old.size();
	curLoadFactor = 100 * amountOfElements / sizeTable_ ;
	//присваивание исходному объекту данных по умолчанию для того чтобы не вызывался деструктор
	old.table = nullptr;
	old.amountOfElements = 0;
	old.sizeTable_ = 0;
	old.curLoadFactor = 0;
}
//оператор присваивания перемещения для класса 
HashTable& HashTable::operator=(HashTable&& old)
{
	if (this != &old)
	{
		freeLists(*(this)->table, this->sizeTable_);
		delete this->table;

		// Copy the data pointer and its length from the   
		// source object.  
		table = old.table;
		sizeTable_ = old.table->size();
		amountOfElements = old.size();
		curLoadFactor = 100 * amountOfElements / sizeTable_;

		// Release the data pointer from the source object so that  
		// the destructor does not free the memory multiple times.  
		old.table = nullptr;
		old.amountOfElements = 0;
		old.sizeTable_ = 0;
		old.curLoadFactor = 0;
	}
	return *this;

}

bool HashTable::contains(const Key& surname) const
{
	size_t const index = hash(surname);
	if (!((*table)[index]))
	{
		return false;
	}
	const auto iter = findByKey(surname, *table);
	if (iter == (*table)[index]->end())
	{
		return false;
	}
	return true;
}

Value& HashTable::at(const Key& surname) const
{
	size_t const index = hash(surname);
	if (!((*table)[index]))
	{
		const std::string errorMessage = "Error: " + surname + " is not in the hashtable\n";
		throw std::out_of_range(errorMessage);
	}
	const auto iter = findByKey(surname, *table);
	if (iter == (*table)[index]->end())
	{
		const std::string errorMessage = "Error: " + surname + " is not in the hashtable\n";
		throw std::out_of_range(errorMessage);
	}
	return iter->second;
}

Value& HashTable::at(const Key& surname)
{
	return const_cast<Value&>(static_cast<const HashTable*>(this)->at(surname));
}

Value& HashTable::operator[](const Key& surname)
{
	Table &tableR = *table;
	size_t const index = hash(surname);
	Value insValue;
	if (!tableR[index])
	{
		insert(surname, insValue);
		return findByKey(surname, *table)->second;
	}
	const List::iterator iter = findByKey(surname, *table);
	if (iter == (*table)[index]->end())
	{
		insert(surname, insValue);
		amountOfElements++;
		return findByKey(surname, *table)->second;
	}
	return iter->second;
}

bool HashTable::insert(const Key& surname, const Value& List)
{
	if (curLoadFactor >= LOAD_FACTOR)
		resize();
	size_t const key = hash(surname);
	Table &tableR = *table;
	Value insValue(List.age, List.weight);
	if (!tableR[key])
	{
		HashTable::List *insList = new HashTable::List;
		(*insList).push_back(make_pair(surname, insValue));
		tableR[key] = insList;
		amountOfElements++;
		updateLoadFactor();
		return true;
	}
	const List::iterator iter = findByKey(surname, *table);
	if (iter != tableR[key]->end())
	{
		iter->second = insValue;
		return true;
	}
	else
	{
		tableR[key]->push_back(make_pair(surname, insValue));
		amountOfElements++;
		updateLoadFactor();
		return true;
	}
	return false;
}

bool HashTable::erase(const Key& surname)
{
	const size_t index = hash(surname);
	if (!((*table)[index]))
	{
		return false;
	}
	const List::iterator iterator = findByKey(surname, *table);
	if (iterator == (*table)[index]->end())
	{
		return false;
	}
	if ((*table)[index]->size() == 1)
	{
		delete (*table)[index];
		(*table)[index] = nullptr;
	}
	else
	{
		(*table)[index]->erase(iterator);
	}
	amountOfElements--;
	return true;
}

void HashTable::resize()
{
	const size_t tempSizeTable = getNextPrimeNumber(sizeTable_);
	const size_t oldSizeTable = sizeTable_;
	size_t tempAmountOfElements = 0;
	Table *tempTable = nullptr;
	sizeTable_ = tempSizeTable;
	try
	{
		tempTable = new Table(tempSizeTable);
		for (size_t i = 0; i != oldSizeTable; i++)
		{
			if (!((*table)[i]))
			{
				continue;
			}
			for (List::iterator iterator = (*table)[i]->begin(); iterator != (*table)[i]->end(); ++iterator)
			{
				size_t newKey = hash(iterator->first);
				Value insValue(iterator->second.age, iterator->second.weight);
				if (!(*tempTable)[newKey])
				{
					List *insList = new List;
					(*insList).push_back(std::make_pair(iterator->first, insValue));
					(*tempTable)[newKey] = insList;
					tempAmountOfElements++;
				}
				else//вставить в конец

				{
					(*tempTable)[newKey]->push_back(std::make_pair(iterator->first, insValue));
					tempAmountOfElements++;
				}
			}
		}
	}
	catch (std::bad_alloc& e)
	{
		freeLists(*tempTable, tempSizeTable);
		delete tempTable;
		tempTable = nullptr;
		throw e;
		sizeTable_ = oldSizeTable;
	}
	//удалить старую
	freeLists(*table, oldSizeTable);
	delete table;

	amountOfElements = tempAmountOfElements;
	table = tempTable;
	updateLoadFactor();
}

void HashTable::clear()
{
	freeLists(*table, sizeTable_);
	table->resize(INITIAL_SIZE);
	sizeTable_ = INITIAL_SIZE;
	amountOfElements = 0;
	updateLoadFactor();
}

void HashTable::swap(HashTable &secondTable)
{
	std::swap(this->amountOfElements, secondTable.amountOfElements);
	std::swap(this->sizeTable_, secondTable.sizeTable_);
	std::swap(this->curLoadFactor, secondTable.curLoadFactor);
	std::swap(this->table, secondTable.table);
}

bool operator==(const HashTable &first, const HashTable &second)
{
	if (&first == &second)
	{
		return true;
	}

	if (first.sizeTable() != second.sizeTable())
	{
		return false;
	}

	if (first.size() != second.size())
	{
		return false;
	}

	HashTable::List::iterator iterFirst, iterSecond;
	for (auto i = 0; i != first.sizeTable_; ++i)
	{
		if ((!(*first.table)[i]) && ((!(*second.table)[i])))
		{
			continue;
		}

		if ((!(*first.table)[i]) || ((!(*second.table)[i])))
		{
			return false;
		}

		if ((*(*first.table)[i]).size() != (*(*second.table)[i]).size())
		{
			return false;
		}
		iterFirst = (*(*first.table)[i]).begin();
		iterSecond = (*(*second.table)[i]).begin();
		while (iterFirst != (*(*first.table)[i]).end() || iterSecond != (*(*second.table)[i]).end())
		{
			if ((iterFirst->first == iterSecond->first)
				&& (iterFirst->second.weight == iterSecond->second.weight)
				&& (iterFirst->second.age == iterSecond->second.age))
			{
				iterFirst++;
				iterSecond++;
				continue;
			}
			return false;
		}
	}
	return true;
}

bool operator!=(const HashTable& first, const HashTable& second)
{
	return (!(first == second));
}

HashTable::Iterator HashTable::begin() const
{
	Table::iterator iterVector;
	std::list<std::pair<Key, Value>> ::iterator iterList;
	int indexVector = 0;
	int indexList = -1;
	if (empty())
	{//ISSUE зачем столько полей содержит твой итератор, которые нигде не используются? Хватило бы таблицы и двух указателей. Или указателя на лист и индекса в векторе 
		return Iterator(table->end(), std::list<std::pair<Key, Value>>::iterator(), table, indexVector, indexList);
	}

	for (indexVector = 0; indexVector != sizeTable_; indexVector++)
	{
		if (!((*table)[indexVector]))
		{
			continue;
		}

		indexList = 0;
		break;
	}
	iterList = (*table)[indexVector]->begin();
	iterVector = (*table).begin();
	std::advance(iterVector, indexVector);
	Iterator ret = Iterator(iterVector, iterList, table, indexVector, indexList);
	return ret;
}

HashTable::Iterator HashTable::end() const //ISSUE метод должен возвращать один и тот же итератор конца независимо от количества элементов в таблице
{
	Table::iterator iterVector;
	std::list<std::pair<Key, Value>> ::iterator iterList;
	int indexVector = 0;
	int indexList = -1;
	if (empty())
	{
		return Iterator(table->end(), std::list<std::pair<Key, Value>>::iterator(), table, indexVector, indexList);
	}

	return Iterator(table->end(), std::list<std::pair<Key, Value>>::iterator(), table, table->size(), indexList);
}

size_t  HashTable::hash(Key str) const
{
	return ((std::hash<std::string>()(str)) % sizeTable_);

}
size_t HashTable::size() const
{
	return amountOfElements;
}
bool HashTable::empty() const
{
	return !amountOfElements;

}

//class Iterator
HashTable::Iterator& HashTable::Iterator::operator++()
{

	for (indexVector; indexVector != iterTable->size(); indexVector++) //ISSUE не пойму, что ты тут делаешь. по сути должен сделать ++listIterator, и если он end(), то взять следующий непустой лист
	{
		if ((*iterTable)[indexVector])
		{
		
			iterVector = (*iterTable).begin();
			std::advance(iterVector, indexVector);
			iterList = (*iterTable)[indexVector]->begin();

			if ((indexList + 1) <= (*iterTable)[indexVector]->size() - 1)
			{
				std::advance(iterList, ++indexList);
				return *this;
			}
			indexList = -1;
		}
	}

	iterVector = std::vector <std::list<std::pair<Key, Value>>*>::iterator(); //ISSUE возвращаемый последний итератор не равен end()
	iterList = std::list<std::pair<Key, Value>>::iterator();
	return *this;
}


const HashTable::HTPair& HashTable::Iterator::operator*() const
{
		return (*iterList);
	
}



HashTable::Iterator& HashTable::Iterator::operator=(const Iterator& ci)
{
	if (this != &ci)
	{
		this->amountOfElements = ci.amountOfElements;
		this->iterTable = ci.iterTable;
		this->indexList = ci.indexList;
		this->indexVector = ci.indexVector;
		this->iterList = ci.iterList;
		this->iterVector = ci.iterVector;
	}
	return *this;
}

bool HashTable::Iterator::operator==(const Iterator& ci) const //ISSUE достаточно сравнить указатели итераторов на равенство, если они равны, значит указывают на один и тот же объект
{
	
	while(true)
	{
		if(this->indexVector!= ci.indexVector)
		{
			return false;
		}

		if (this->indexList != ci.indexList)
		{
			return false;
		}

		
		if((*iterTable).size() != ci.iterTable->size())
		{
			return false;
		}
		if( iterList!= ci.iterList)
		{
			return false;
		}


		HashTable::List::iterator iterFirst, iterSecond;
		for(int i = 0;i!=(*iterTable).size();i++)
		{
			if ( ((*iterTable)[i]) && (*ci.iterTable)[i])
			{
				if ((*iterTable)[i]->size() != (*ci.iterTable)[i]->size())
				{
					return false;
				}

				iterFirst = (*iterTable)[i]->begin();
				iterSecond = (*ci.iterTable)[i]->begin();
				while (iterFirst != (*iterTable)[i]->end() || iterSecond != (*ci.iterTable)[i]->end())
				{
					if ((iterFirst->first == iterSecond->first)
						&& (iterFirst->second.weight == iterSecond->second.weight)
						&& (iterFirst->second.age == iterSecond->second.age))
					{
						iterFirst++;
						iterSecond++;
						continue;
					}
					return false;
				}
			}
		}
	break;
	}
	return true;
}

bool HashTable::Iterator::operator!=(const Iterator& ci) const
{
	return !(*this == ci);
}
