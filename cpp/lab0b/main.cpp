#include "header.h"
using namespace std;

int main(int argc, char** argv)
{
	setlocale(LC_ALL, "rus");
	const string inputFile = argv[1];
	const string outputFile = argv[2];

	if (argc != 3)
	{
		cout << "Usage: lab0b.exe <inputFile> <outputFile>" << "\n";
		return EXIT_FAILURE;
	}
	ifstream in(inputFile, ios::in);
	if (!in) 
	{
		cout << "Can't open input file" << "\n";
		return EXIT_FAILURE;
	}
	
	string curString;
	list<string> data;
	while (getline(in, curString))
	{
		data.push_back(curString);
	}
	Sort::sortStrings(data);

	ofstream out(outputFile, ios::out);
	if (!out)
	{
		cout << "Can't open output  file" << "\n";
		return EXIT_FAILURE;
	}

	while(!data.empty())
	{
		out << data.front() << endl;
		data.pop_front();
	}


	in.close();
	out.close();
	return EXIT_SUCCESS;
}