template <typename  T> class AutoPtr
{
	T* ptr;

public:
	AutoPtr(T* ptr)
		:ptr(ptr){}

	~AutoPtr()
	{
		delete ptr;
	}

	AutoPtr(AutoPtr& ap)
		:ptr(ap)
	{
		ap = nullptr;
	}

	AutoPtr& operator =(AutoPtr& ap)
	{
		if (this == &ap)
		{
			return  *this;
		}
		delete this->ptr;
		this->ptr = ap.ptr;
		ap.ptr = nullptr;
		return  *this;
	}

	T* operator ->()
	{
		return this->ptr;
	}
	
	T& operator *()
	{
		return (*this->ptr);
	}

	void reset(T* ptr)
	{
		delete this->ptr;
		this->ptr = ptr;
	}

	T* get()
	{
		return *ptr;
	}

};