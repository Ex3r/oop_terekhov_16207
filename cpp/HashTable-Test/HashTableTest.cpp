﻿#include <gtest/gtest.h>
#include "../lab1/hashTable.h"

//#include <gtest/gtest.h>
//
//int main(int argc, char * argv[])
//{
//	testing::InitGoogleTest(&argc, argv);
//	return RUN_ALL_TESTS();
//}

const int INITIAL_SIZE = 7;
const int EMPTY = 0;
const int n = 1000;
class HashTableTest :public testing::Test
{
	
};

class IteratorsTest :public testing::Test
{

};

TEST_F(HashTableTest, constructor)
{
	ASSERT_NO_THROW(HashTable a);
}

TEST_F(HashTableTest, copyConstructor_empty)
{
	HashTable a1;
	ASSERT_NO_THROW(HashTable a3(a1));
}

TEST_F(HashTableTest, copyConstructor_notempty)
{
	HashTable a1;
	for (unsigned int i = 0; i<n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
	}
	ASSERT_NO_THROW(HashTable a3(a1));
}

TEST_F(HashTableTest, assignmentOperator_empty)
{
	HashTable a1;
	ASSERT_NO_THROW(HashTable a3 = a1);
}

TEST_F(HashTableTest, assignmentOperator_notempty)
{
	HashTable a1,a3;
	for (unsigned int i = 0; i < n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
	}

	ASSERT_NO_THROW(a3 = a1);
	EXPECT_EQ(a1.size(), a3.size());
}


TEST_F(HashTableTest, clear_empty)
{
	HashTable a1;
	ASSERT_NO_THROW(a1.clear());
	EXPECT_EQ(a1.table->size(), INITIAL_SIZE);
	EXPECT_EQ(a1.size(), EMPTY);
}

TEST_F(HashTableTest, clear_notempty)
{
	HashTable a1;
	for (unsigned int i = 0; i < n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
	}
	ASSERT_NO_THROW(a1.clear());
	EXPECT_EQ(a1.table->size(), INITIAL_SIZE);
	EXPECT_EQ(a1.size(), EMPTY);
}

TEST_F(HashTableTest, insert_empty)
{
	HashTable a1;
	EXPECT_TRUE(a1.insert("test", {18,18}));
	EXPECT_EQ(a1.size(), 1);
	EXPECT_TRUE(a1.contains("test"));
}

TEST_F(HashTableTest, insertTheSameKeys)
{
	HashTable a1;
	EXPECT_TRUE(a1.insert("test", { 18,18 }));
	EXPECT_EQ(a1.size(), 1);
	EXPECT_TRUE(a1.insert("test", { 20,18 }));
	EXPECT_TRUE(a1.contains("test"));
	Value temp = a1.at("test");
	EXPECT_EQ(temp.age, 20);
	EXPECT_EQ(temp.weight, 18);

}

TEST_F(HashTableTest, insertWithResize)
{
	HashTable a1;
	for (unsigned int i = 0; i<n; i++)
	{
		EXPECT_TRUE(a1.insert("test" + std::to_string(i), { i, i }));
	}

	for (int i = 0; i<n; i++)
	{
		EXPECT_TRUE(a1.contains("test"+std::to_string(i)));
	}

	EXPECT_EQ(a1.size(), n);
	EXPECT_FALSE(a1.empty());
}

TEST_F(HashTableTest, erase_empty)
{
	HashTable a1;
	EXPECT_FALSE(a1.erase("test"));
}

TEST_F(HashTableTest, erase)
{
	HashTable a1;
	for (unsigned int i = 0; i<n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
	}

	for (unsigned int i = 0; i<n; i++)
	{
		EXPECT_TRUE(a1.erase("test" + std::to_string(i)));
	}

	EXPECT_EQ(a1.size(), EMPTY);
	EXPECT_TRUE(a1.empty());
}

TEST_F(HashTableTest, at_notfound)
{
	HashTable a1;
	ASSERT_ANY_THROW(a1.at("test"));
}

TEST_F(HashTableTest, at_found)
{
	HashTable a1;
	for (unsigned int i = 0; i<n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
	}

	for (unsigned int i = 0; i<n; i++)
	{
		ASSERT_NO_THROW(a1.at("test" + std::to_string(i)));
	}
}

TEST_F(HashTableTest, operIndex_empty)
{
	HashTable a1;
	EXPECT_EQ(a1.size(), EMPTY);
	Value temp = a1["test"];
	EXPECT_EQ(temp.age, 20);
	EXPECT_EQ(temp.weight, 18);
	EXPECT_EQ(a1.size(), 1);
}

TEST_F(HashTableTest, operIndex_notempty)
{
	HashTable a1;
	for (unsigned int i = 0; i<n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
	}
	Value temp = a1["test500"];
	EXPECT_EQ(temp.age, 500);
	EXPECT_EQ(temp.weight, 500);
}

TEST_F(HashTableTest, swap)
{
	HashTable a1, a2;
	a1.insert("testA1", { 10,10 });
	a2.insert("testA2_1", { 20,20 });
	a2.insert("testA2_2", { 30,30 });
	a1.swap(a2);
	EXPECT_EQ(a1.size(), 2);
	EXPECT_EQ(a2.size(), 1);
	EXPECT_TRUE(a1.contains("testA2_1"));
	EXPECT_TRUE(a1.contains("testA2_2"));
	EXPECT_TRUE(a2.contains("testA1"));
}

TEST_F(HashTableTest, comparisonOperator_1)
{
	HashTable a1,a2;
	EXPECT_TRUE(a1 == a1);
	for (unsigned int i = 0; i<n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
		a2.insert("test" + std::to_string(i), { i, i });
	}

	EXPECT_TRUE(a1 == a2);
	EXPECT_FALSE(a1 != a2);

}

TEST_F(HashTableTest, comparisonOperator_2)
{
	HashTable a1, a2;
	for (unsigned int i = 0; i<n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
		a2.insert("test" + std::to_string(i), { i, i });
	}
	a2.erase("test999");
	EXPECT_FALSE(a1 == a2);
	EXPECT_TRUE(a1 != a2);

}

HashTable createHashTableEmpty()
{
	HashTable retHashTable;
	return retHashTable;
}

HashTable createHashTableNotEmpty()
{
	HashTable retHashTable;
	for (unsigned int i = 0; i<n; i++)
	{
		retHashTable.insert("test" + std::to_string(i), { i, i });
	}
	return retHashTable;
}

TEST_F(HashTableTest, moveConstructor_1)
{
	//HashTable a1 = createHashTable();// срабатывает конструктор перемещения
	//HashTable a2 = a1;               // срабатывает конструктор копирования
	//a2 = createHashTable();          // срабатывает конструктор перемещения, затем оператор перемещения
	//a2 = a1;                         // срабатывает оператор присваивания

	// срабатывает конструктор перемещения
	ASSERT_NO_THROW(HashTable a1 = createHashTableEmpty()); 
}

TEST_F(HashTableTest, moveConstructor_2)
{
	ASSERT_NO_THROW(HashTable a1 = createHashTableNotEmpty());
}

TEST_F(IteratorsTest, iteratorTest_1)
{
	HashTable a1;
	for (unsigned int i = 0; i<n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
	}
	int k = 0;
	ASSERT_NO_THROW(for (const auto elem : a1) {
		std::cout << elem.first << " " << elem.second.age << " " << elem.second.weight << "\n";
		k++;
	});
	EXPECT_EQ(k, n);
}

TEST_F(IteratorsTest, iteratorTest_2)
{
	HashTable a1;
	int k = 0;
	ASSERT_NO_THROW(for (const auto elem : a1) {
		std::cout << elem.first << " " << elem.second.age << " " << elem.second.weight << "\n";
		k++;
	});
	EXPECT_EQ(k, 0);
}

TEST_F(IteratorsTest, iteratorTest_3)
{
	HashTable a1;
	for (unsigned int i = 0; i<n; i++)
	{
		a1.insert("test" + std::to_string(i), { i, i });
	}
	HashTable::Iterator it(a1);
	for (unsigned int i = 0; i<100; i++)
	{
		++it;
	}
	HashTable::Iterator it2 = it;
	EXPECT_EQ((*it).first, (*it2).first);
	EXPECT_EQ((*it).second.weight, (*it2).second.weight);
	EXPECT_EQ((*it).second.age, (*it2).second.age);

	
}
