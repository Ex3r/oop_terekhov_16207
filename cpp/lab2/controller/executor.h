#ifndef EXECUTOR_H
#define EXECUTOR_H
#include <string>
#include "strategy.h"
namespace model {
	//ISSUE ����� ������� ��������� ����: ��� ������� ��� ���(����), ���������(������ ��� ������ �����))
	//ISSUE FOR ISSUE ��� � �� �������
	struct CommandParameters
	{
		CommandParameters(int commandNum = -1, std::string firstParam = { "???" }, std::string secondParam = { "???" }, std::string errorMessage = {"success"})
			: commandNum(commandNum), firstParam(firstParam), secondParam(secondParam), errorMessage(errorMessage) {}
		int commandNum;
		std::string firstParam;
		std::string secondParam;
		std::string errorMessage;
	};
}

namespace controller
{
	class Executor
	{
	public:
		Executor() {};
		int executeCommand(model::CommandParameters& command, Strategy& str);
	};
}
#endif // EXECUTOR_H
