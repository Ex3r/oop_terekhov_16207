#include "deserializer.h"
#include "../model/cell.h"

namespace controller
{
	size_t getSize(std::ifstream& file)
	{
		file.seekg(0, std::ios_base::end);
		const size_t fileSize = file.tellg();
		file.seekg(0, std::ios_base::beg);
		return fileSize;
		
	}
    bool isDataCorrect(std::ifstream& file,int size)
	{
		char buffer[11];
		std::string temp1;
		for (size_t i = 0; i !=size; ++i)
		{
			file.getline(buffer, size+1);

			for (size_t j = 0; j != size; ++j)
			{
				temp1.push_back(buffer[j]);
				if ((temp1 != "0" ) && (temp1 != "1"))
				{
					file.close();
					return false;
				}
				temp1.erase();
			}
		}
		file.seekg(0, std::ios_base::beg);
		return true;
	}

	bool Deserializer::load(std::vector<std::vector<model::Cell>>& world, std::string fileName, std::string& output)
	{
		std::ifstream inputFile(fileName, std::ios::in);
		if (!inputFile)
		{
			output = "Error: Could not open  input file " + fileName;
			std::cout << "Error: Could not open  input file " + fileName + "\n";
			return true;
		}

		std::string errorMesage =  "Error: Incorrect CellModification in file:";
		if (getSize(inputFile)!= (world.size() * world[0].size() +sizeof("\n") *world.size()))
		{
			output = errorMesage + " File size is different from standart";
			std::cout << (errorMesage + " File size is different from standart\n");
			inputFile.close();
			return true;
		}

		if (!isDataCorrect(inputFile, world.size()))
		{
			output = errorMesage + " In the file must be only characters \'0\' and \'1\'";
			std::cout << (errorMesage + " In the file must be only characters \'0\' and \'1\'\n");
			inputFile.close();
			return true;
		}
		char temp;
		std::string tempStr;
		for (size_t i = 0; i != world.size(); ++i)
		{
			for (std::vector<model::Cell>::iterator iter = world[i].begin(); iter != world[i].end(); ++iter)
			{
				inputFile.get(temp);
				tempStr.push_back(temp);
				(*iter) = static_cast<bool>(std::stoi(tempStr));
				tempStr.erase();
			}
			inputFile.get(temp);
		}

		inputFile.close();
		return false;
	}

}
