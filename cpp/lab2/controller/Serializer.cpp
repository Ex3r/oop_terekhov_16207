
#include "serializer.h"
namespace controller
{
	inline bool isFileExist(std::string fileName)
	{
		std::ifstream infile(fileName);
		return infile.good();
	}

	char askUserForRewrite(std::string fileName, char testAnswer)
	{
		bool answered = false;
		char answer;
		if(testAnswer == 'n')
		{
			return 'n';
		}
		if (testAnswer == 'y')
		{
			return 'y';
		}
		while (!answered && testAnswer!='n')
		{
			std::cin.get(answer);
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			if ((answer == 'Y') || (answer == 'y') || (answer == 'N') || (answer == 'n'))
			{
				answered = true;
				break;
			}
		}
		return answer;
	}

	inline void write(std::ofstream& outputFile, std::vector<std::vector<model::Cell>>& world)
	{
		for (size_t i = 0; i != world.size(); ++i)
		{
			for (std::vector<model::Cell>::iterator iter = world[i].begin(); iter != world[i].end(); ++iter)
			{
				outputFile << (*iter).getState();
			}
			outputFile << "\n";
		}
		outputFile.close();
	}
	bool Serializer::save(std::vector<std::vector<model::Cell>>& world, std::string fileName, std::string& output, char testAnswer)
	{
		if( isFileExist(fileName))
		{
			output = "Warning: File " + fileName + " already exists. Rewrite? Y/N";
			std::cout << "Warning: File " + fileName + " already exists. Rewrite? Y/N\n";
			const char answer = askUserForRewrite(fileName, testAnswer);

			if ((answer == 'N') || (answer == 'n'))
			{
				return false;
			}
				std::ofstream outputFile(fileName, std::ios::trunc);
				write(outputFile, world);
				output = "The game was successfully saved in file " + fileName;
				std::cout << "The game was successfully saved in file " + fileName + "\n";
				return false;
		}

		std::ofstream outputFile(fileName, std::ios::out);
		if (!outputFile)
		{
			output = "Error: Could not create  output file " + fileName;
			std::cout << "Error: Could not create  output file " + fileName + "\n";
			return true;
		}
		write(outputFile, world);
		output = "The game was successfully saved in file " + fileName;
		std::cout << "The game was successfully saved in file " + fileName + "\n";
		return false;
	}


}
