﻿#include "executor.h"
#include "../view/consoleView.h"
#include "../model/history.h"
#include "strategy.h"
#include "deserializer.h"
#include "serializer.h"
#include "game.h"

namespace controller
{
	void reset(Strategy& str)
	{
		str.getHistory().freeHistory();
		str.setStep(0);
		str.getWorld().clearField();
		str.setNumOfAliveCells(0);
		view::ConsoleView::printField(str.getWorld().getField());
		view::ConsoleView::printStepCounter(str.getStep());
	}

	void set(model::CommandParameters& command, Strategy& str)
	{
		const int firstParam = std::stoi(command.firstParam);
		const int secondParam = std::stoi(command.secondParam);
		str.getWorld().getField()[secondParam][firstParam].setState(true);

		const model::CellModification changedCells(secondParam, firstParam, true);
		str.getHistory().writeToHistory(str.getStep(), changedCells);

		view::ConsoleView::printField(str.getWorld().getField());
		view::ConsoleView::printStepCounter(str.getStep());
	}

	void clear(model::CommandParameters& command, Strategy& str)
	{
		const int firstParam = std::stoi(command.firstParam);
		const int secondParam = std::stoi(command.secondParam);

		str.getWorld().getField()[secondParam][firstParam].setState(false);
		const model::CellModification changedCells(secondParam, firstParam, false);
		str.getHistory().writeToHistory(str.getStep(), changedCells);

		view::ConsoleView::printField(str.getWorld().getField());
		view::ConsoleView::printStepCounter(str.getStep());
	}
	void step(model::CommandParameters& command, Strategy& str)
	{
		const int firstParam = std::stoi(command.firstParam);
		str.step(firstParam);
		view::ConsoleView::printField(str.getWorld().getField());
		view::ConsoleView::printStepCounter(str.getStep());
	}

	void back(Strategy& str)
	{
		str.back();
		view::ConsoleView::printField(str.getWorld().getField());
		view::ConsoleView::printStepCounter(str.getStep());
	}
	void save(model::CommandParameters& command, Strategy& str)
	{
		const std::string firstParam = command.firstParam;
		std::string output;
		Serializer::save(str.getWorld().getField(), firstParam, output);
	}

	void load(model::CommandParameters& command, Strategy& str)
	{
		const std::string firstParam = command.firstParam;
		std::string output;
		if(Deserializer::load(str.getWorld().getField(), firstParam , output))
		{
			system("pause");
		}

		str.getHistory().freeHistory();
		str.setStep(0);

		view::ConsoleView::printField(str.getWorld().getField());
		view::ConsoleView::printStepCounter(str.getStep());
	}

	void help()
	{
		view::ConsoleView::printHelp();
	}

	void exit(Strategy& str)
	{
		str.finishTheGame();
	}
	int controller::Executor::executeCommand(model::CommandParameters& command, Strategy& str)
	{
		const int commandNum = command.commandNum;
		switch (commandNum)
		{
		case 0:
		{
			reset(str);
			break;
		}
		case 1:
		{
			set(command, str);
			break;
		}
		case 2:
		{
			clear (command, str);
			break;
		}
		case 3:
		{
			step(command, str);
			break;
		}

		case 4:
		{
			back(str);
			break;
		}
		case 5:
		{
			save(command, str);
			break;
		}
		case 6:
		{
			load(command, str);
			break;
		}
		
		case 7:
		{	
			help();
			break;
;		}

		case 8:
		{	
			exit(str);
			break;
		
		}
		default:
			break;
		}
		return 0;
	}
}
