#ifndef STRATEGY_H
#define STRATEGY_H
#include "../model/world.h"
#include "../model/history.h"

namespace controller
{
	class Strategy
	{
		void updateAliveNeighboursCountForEachCell(); 
		int countNeighbours(unsigned int x, unsigned int y); 
		void checkForChangedCellState();
		bool exit;
		int numberOfAliveCells;
		bool noOnCellHasChangedItsState;
		size_t stepCounter;
		model::World world;
		model::History history;
	public:
		Strategy(model::World w, model::History h);
		model::World& getWorld();
		model::History& getHistory();
		size_t getStep();
		void setStep(size_t newStep);
		void setCellInvariably(bool state = true);
		bool getCellInvariably();
		bool getInfoAboutExit();
		void finishTheGame();
		int countAliveCells();
		int getNumOfAliveCells() const;
		void setNumOfAliveCells(int num);
		void step(size_t value);
		void back();	
	};

}
#endif // STRATEGY_H
