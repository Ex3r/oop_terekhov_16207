﻿#include "strategy.h"

namespace controller
{
	Strategy::Strategy(model::World w, model::History h)
		:world(w), history(h)
	{
		exit = false;//ISSUE по умолчанию полям итак присвоятся эти значения. можно не писать
		stepCounter = 0;
		noOnCellHasChangedItsState = false;
		numberOfAliveCells = 0;
	}

	model::World& Strategy::getWorld()
	{
		return world;
	}

	model::History& Strategy::getHistory()
	{
		return history;
	}

	size_t Strategy::getStep()
	{
		return stepCounter;
	}

	void Strategy::setStep(size_t newStep)
	{
		stepCounter = newStep;
	}

	void Strategy::setCellInvariably(bool state)
	{
		noOnCellHasChangedItsState = state;
	}

	bool Strategy::getCellInvariably()
	{
		return noOnCellHasChangedItsState;
	}

	bool Strategy::getInfoAboutExit()
	{
		return exit;
	}

	void Strategy::finishTheGame()
	{
		exit = true;
	}


	int Strategy::getNumOfAliveCells() const
	{
		return numberOfAliveCells;
	}

	void Strategy::setNumOfAliveCells(int num)
	{
		numberOfAliveCells = num;
	}

	

	int Strategy::countAliveCells()
	{
		numberOfAliveCells = 0;
		for (size_t i = 0; i != world.getField().size(); ++i)
		{
			for (std::vector<model::Cell>::iterator iter = (world.getField()[i]).begin(); iter != (world.getField()[i]).end(); ++iter)
			{
				if ((*iter).getState() == true)
				{
					numberOfAliveCells++;
				}
			}
		}
	return numberOfAliveCells;
	}

	/*Подсчёт соседей клетки*/
	int Strategy::countNeighbours(unsigned int x, unsigned int y)
	{
		int count = 0;

		if ((this->world.getField()[(x + 1 + this->world.width) % this->world.width])[y].getState())
		{
			count++;
		}
		if (this->world.getField()[(x + 1 + this->world.width) % this->world.width][(y + 1 + this->world.height) % this->world.height].getState())
		{
			count++;
		}
		if (this->world.getField()[x][(y + 1 + this->world.height) % this->world.height].getState())
		{
			count++;
		}
		if (this->world.getField()[x][(y - 1 + this->world.height) % this->world.height].getState())
		{
			count++;
		}
		if (this->world.getField()[(x + 1 + this->world.width) % this->world.width][(y - 1 + this->world.height) % this->world.height].getState())
		{
			count++;
		}
		if (this->world.getField()[(x - 1 + this->world.width) % this->world.width][y].getState())
		{
			count++;
		}
		if (this->world.getField()[(x - 1 + this->world.width) % this->world.width][(y - 1 + this->world.height) % this->world.height].getState())
		{
			count++;
		}
		if (this->world.getField()[(x - 1 + this->world.width) % this->world.width][(y + 1 + this->world.height) % this->world.height].getState())
		{
			count++;
		}

		return count;
	}
	/*Подсчёт соседей всего мира*/
	void Strategy::updateAliveNeighboursCountForEachCell()
	{
		for (size_t i = 0; i != world.width; ++i)
		{
			for (size_t j = 0; j != world.height; ++j)
			{
				world.getField()[i][j].setAmountAliveNeigbours(countNeighbours(i, j));
			}
		}
	}
	void Strategy::step(size_t value)
	{
		//global loop
		std::cout << "Generation of Cells .....";
		for (size_t g = 0; g != value; ++g)
		{
			++stepCounter;
			updateAliveNeighboursCountForEachCell();
			for (size_t i = 0; i != world.width; ++i)
			{
				for (size_t j = 0; j != world.height; ++j)
				{
					//была мертва и есть 3 соседа =>жива
					if ((world.getField()[i][j].getState() == 0) && (world.getField()[i][j].getAmountAliveNeigbours() == 3))
					{
						
						world.getField()[i][j].setState(true);
						model::CellModification changedCells(i, j, true);
						history.writeToHistory((stepCounter), changedCells);
					}
					//была жива, но соседей менее 2 =>умирает
					if ((world.getField()[i][j].getState() == 1) && (world.getField()[i][j].getAmountAliveNeigbours() < 2))
					{
						world.getField()[i][j].setState(false);
						model::CellModification changedCells(i, j, false);
						history.writeToHistory((stepCounter), changedCells);
					}
					//была жива, имеет более 3-x соседей =>умирает
					if ((world.getField()[i][j].getState() == 1) && (world.getField()[i][j].getAmountAliveNeigbours() > 3))
					{
						world.getField()[i][j].setState(false);
						model::CellModification changedCells(i, j, false);
						history.writeToHistory((stepCounter), changedCells);
					}
					//обнуление числа живых соседей
					world.getField()[i][j].setAmountAliveNeigbours(0);
				}
			}
		}

	checkForChangedCellState();
	}

	void Strategy::checkForChangedCellState()
	{
			static const int width = world.width;
			static const int height = world.height;
			static std::vector<std::vector<int>> stateOfCells(width, std::vector<int>(height));
			noOnCellHasChangedItsState = false;
			if (stepCounter != 0)
			{
				noOnCellHasChangedItsState = true;
				for (int i = 0; i != world.width && noOnCellHasChangedItsState; ++i)
				{
					for (int j = 0; j != world.height; ++j)
					{
						if (stateOfCells[i][j] != world.getField()[i][j].getState())
						{
							noOnCellHasChangedItsState = false;
							break;
						}
					}
				}
			}
			//пересчёт старого состояния клеток
			for (int i = 0; i != world.width; ++i)
			{
				for (int j = 0; j != world.height; ++j)
				{
					stateOfCells[i][j] = world.getField()[i][j].getState();
				}
			}

		}
	

	void Strategy::back()
	{
		std::list<model::CellModification> curRecord;
		if (history.getStorage()->size() == 0)
		{
			std::cout << "You can no longer use the command \"back\". The game is in the original state\n";
			return;
		}

		curRecord = history.readFromHistory(stepCounter);
		for(std::reverse_iterator<std::list<model::CellModification>::iterator> iter = curRecord.rbegin();iter!= curRecord.rend();++iter)
		{
			if (iter->state ==false)
			{
				world.getField()[iter->x][iter->y].setState(true);
				continue;
			}

			if (iter->state == true)
			{
				world.getField()[iter->x][iter->y].setState(false);
			}
		}
		checkForChangedCellState();
		stepCounter--;
	}

}
	
