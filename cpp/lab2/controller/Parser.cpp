﻿#include "parser.h"
#include <string>
#include <cctype>
namespace controller
{
//Удаление пробелов с начала и конца слова
 std::string trim(std::string& str)
{
	str.erase(0, str.find_first_not_of(' '));         //prefixing spaces
	str.erase(str.find_last_not_of(' ') + 1);         //surfixing spaces
	return str;
}
//Преобразование символов в нижний регистр
 std::string tolower(std::string& s) {
	std::transform(s.begin(), s.end(), s.begin(),
		[](unsigned char c) { return std::tolower(c); } // correct
	);
	return s;
}

/*Найдена ли подстрока в строке
	*индекс
	* иначе -1
	*/
inline int indexOf(std::string s, std::string substring)
{
	int index = -1;
	index = s.find(substring);
	return index;

}
//Анализ кавычек
inline int controlQuotes(std::string s, std::string substring, size_t pos = 0)
{
	int count = 0;
	if (int tmp = s.find(substring))
	{
		if (tmp == std::string::npos)
		{
			return 0;
		}
		if (tmp == s.length() - 1)
		{
			return 0;
		}

		if(tmp!=4)
		{
			return -1;
		}
		
	}
	pos = s.find(substring,pos);
	if (pos)
	{
		count++;
		if (pos!= 4)
		{
			return (count = -1);
		}
	}
	while (pos != std::string::npos)
	{
		if ((count ==2) && ((pos+1) < s.size()))
		{
			count = -2;
		}
		pos= s.find(substring, pos + substring.size());
		if (pos && (pos != std::string::npos))
		{
			count++;
		}
	}
	return count;

}
//Перевод сивола в число
int charToDigit(int c) {
	if (c >= 'a' && c <= 'j')
		return c - 'a' + 10;
	if ('0' <= c && c <= '9')
		return c - '0';
	return -1;
}
//проверка: явл ли символ буквой
bool isAlpha(int value)
{
	if (value>=10 && value<=19)
	{
		return true;
	}
	return false;
}
//проверка: явл ли символ числом
bool isDigit(int value)
{
	if (value >= 0 && value <= 9)
	{
		return true;
	}
	return false;
}
//печать подсказки
std::string printHint(std::string command)
{
	std::cout << "Maybe you meant \"" + command + "\" enter \"help\" to get the detailed information about this command\n";
	return "Maybe you meant \"" + command + "\" enter \"help\" to get the detailed information about this command";
}

void checkResetAndBack(std::string command, int commandNum,  model::CommandParameters&  insCommand)
 {
	if (command == possibleCommands[commandNum])
	{
		insCommand.commandNum = commandNum;
	}
	else
	{
		insCommand.errorMessage = printHint((possibleCommands[commandNum]));
	}
 }

void checkSetAndClear(std::string command, int commandNum, int index, model::CommandParameters&  insCommand)
 {
	int tempFirstParam;
	int tempSecondParam;
	int tempIndex = index + possibleCommands[commandNum].length() + 1;

	if (command.length() > (command.length() + 3))
	{
		insCommand.errorMessage = printHint(possibleCommands[commandNum]);
		return;
	}

	if (command[tempIndex] == ' ')
	{
		tempIndex++;
	}

	if (!isAlpha(tempFirstParam = charToDigit(command[tempIndex])))
	{
		insCommand.errorMessage = "Incorrect usage \"" + possibleCommands[commandNum] + "\" : after " + possibleCommands[commandNum] + " must be letter from \'A\' to \'J\'";
		std::cout << insCommand.errorMessage << std::endl;
		return;
	}

	if (!isDigit(tempSecondParam = charToDigit(command[++tempIndex])))
	{
		insCommand.errorMessage = "Incorrect usage \"" + possibleCommands[commandNum] + "\" : after " + possibleCommands[commandNum] + command[tempIndex - 1] + " must be digit from \'0\' to \'9\'";
		std::cout << insCommand.errorMessage << std::endl;
		return;
	}

	if (tempIndex != command.length() - 1)
	{
		insCommand.errorMessage = printHint((possibleCommands[commandNum]));
		return;
	}
	insCommand.commandNum = commandNum;
	insCommand.firstParam = std::to_string(tempFirstParam - 10);
	insCommand.secondParam = std::to_string(tempSecondParam);
 }

void checkStep(std::string command, int commandNum, int index, model::CommandParameters&  insCommand)
 {
	int tempIndex = index + possibleCommands[commandNum].length() + 1;
	if (command[tempIndex] == ' ')
	{
		tempIndex++;
	}
	std::string errorStr = "Incorrect usage \"step\" : after step";
	bool error = false;
	bool firstIteration = true;
	for (std::string::iterator iter = command.begin() + tempIndex; iter != command.end() && !error; ++iter)
	{
		if (firstIteration)
		{
			if ((*iter) == '0')
			{
				error = true;
				break;
			}
			firstIteration = false;
		}
		if (!isDigit(charToDigit((*iter))))
		{

			error = true;
			break;
		}
		errorStr += (*iter);
	}
	if (error)
	{
		errorStr += " must be digit from \'0\' to \'9\' exept \'0\' in the first position";
		insCommand.errorMessage = errorStr;
		std::cout << insCommand.errorMessage << std::endl;
		return;
	}


	command.erase(std::remove(command.begin(), command.end(), ' '), command.end()); //удаляю все пробелы

																		//если после step нет параметра
	if (command.size() == possibleCommands[commandNum].size())
	{
		insCommand.firstParam = std::to_string(1);
	}
	else
	{
		command.erase(command.begin(), command.begin() + possibleCommands[commandNum].size());
		insCommand.firstParam = command;
	}
	insCommand.commandNum = commandNum;
 }

void checkSaveAndLoad(std::string command, int commandNum, model::CommandParameters&  insCommand)
 {
	command.erase(std::remove(command.begin(), command.end(), ' '), command.end()); //удаляю все пробелы
	const int code = controlQuotes(command, "\"");
	if (code == 0)
	{
		insCommand.errorMessage = "Before the filename  should be opening quotation mark";
		std::cout << insCommand.errorMessage << std::endl;
		return;
	}
	if (code == -1)
	{
		insCommand.errorMessage = "Before the opening quotation mark should be nothing except \"" + possibleCommands[commandNum] + "\"";
		std::cout << insCommand.errorMessage << std::endl;
		return;
	}

	if (code == -2)
	{
		insCommand.errorMessage = "After the closing quotation there should be nothing except \"" + possibleCommands[commandNum] + "\"";
		std::cout << insCommand.errorMessage << std::endl;
		return;
	}

	if (code != 2)
	{
		insCommand.errorMessage = "Must be two quotes, but found: " + std::to_string(code);
		std::cout << insCommand.errorMessage << std::endl;
		return;
	}
	insCommand.firstParam = "";
	for (std::string::iterator iter = command.begin() + possibleCommands[commandNum].size() + 1; iter != command.end() - 1; ++iter)
	{
		insCommand.firstParam += (*iter);
	}
	insCommand.commandNum = commandNum;
 }
model::CommandParameters Parser::parseCommand(std::string command)
{

	int amountOfCommandsInOne = 0;
	int firstOccurrenceNumber = -1;
	int commandNum = -1;
	model::CommandParameters insCommand;
	//Начальные преобразования строки для последующего анализа
	trim(command);
	std::string temp = tolower(command);
	static int sizePosCommands = sizeof(possibleCommands) / sizeof(possibleCommands[0]);

	//Поиск на соответствие командам
	for (size_t i = 0; i != sizePosCommands; i++)
	{
		firstOccurrenceNumber = indexOf(temp, possibleCommands[i]);
		if (firstOccurrenceNumber != -1)
		{
			amountOfCommandsInOne++;
			commandNum = i;
		}
	}

	//Печать предупреждения о нескольких возможных командах, введённых как одну
	if (amountOfCommandsInOne > 1)
	{
		if ((indexOf(temp, "reset")) == -1)
		{
			insCommand.errorMessage = "Too many different commands in one line(" + std::to_string(amountOfCommandsInOne) + ")";
			std::cout << insCommand.errorMessage << std::endl;;
			return insCommand;
		}
		else
		{
			--commandNum;//потому что SET and reSET
		}
	}


	switch (commandNum)
	{
		//reset
		//back
		case 0:
		case 4:
		{
			checkResetAndBack(temp, commandNum, insCommand);
			return insCommand;
		}
		//set XY
		//clear XY
		case 1:
		case 2:
		{
			checkSetAndClear(temp, commandNum, firstOccurrenceNumber, insCommand);
			return insCommand;
		}
		//step N
		case 3:
		{
			checkStep(temp, commandNum, firstOccurrenceNumber, insCommand);
			return insCommand;
		}
		//save “filename”
		//load “filename”
		case 5:
		case 6:
		{
			checkSaveAndLoad(temp, commandNum, insCommand);
			return insCommand;
		}
		case 7:
		//help
			{
			    insCommand.commandNum = commandNum;
				break;
			}

		case 8:
			//exit
		{
			insCommand.commandNum = commandNum;
			break;
		}
		default:
		{
			insCommand.errorMessage = "Unkown command ,try to enter \"help\" for more information";
			std::cout << "Unkown command ,try to enter \"help\" for more information \n";
			break;
		}	
	}
	return insCommand;
}	
}
