#ifndef SERIALIZER_H
#define SERIALIZER_H
#include "../header.h"
#include "../model/cell.h"
namespace  controller
{
	class Serializer
	{
	public:
		Serializer() {};
		~Serializer() {};
		/*
		 * 0 - �������� ����������
		 * 1 - ������ ��� ����������
		 */
		static bool save(std::vector<std::vector<model::Cell>>& world , std::string fileName, std::string& output, char testAnswer='s');
	
	};

}
#endif // SERIALIZER_H
