﻿#include "game.h"
#include "../view/consoleView.h"
#include "executor.h"
#include "parser.h"

namespace controller
{

	/*
	 * false - игра продолжается
	 */
	bool Game::isGameEnd(size_t iteration)
	{	
		this->strategy.countAliveCells();
		exit = ( (strategy.getInfoAboutExit() == true) || (static_cast<bool>(strategy.getNumOfAliveCells()) ==false && (strategy.getStep() !=0 )) 
			|| strategy.getCellInvariably() == true);
		return exit;
	}


	Game::Game(Strategy str, Parser par, Executor exec)
		:strategy(str),parser(par),executor(exec)
	{
		exit = false;
	}

	void Game::start()
	{
		view::ConsoleView::printField(strategy.getWorld().getField());
		view::ConsoleView::printStepCounter(strategy.getStep());
		std::string input;
		model::CommandParameters temp;
		size_t iteration = 0;

		while (!isGameEnd(iteration))
		{
			std::getline(std::cin, input);
			temp = parser.parseCommand(input);
			executor.executeCommand(temp, strategy);
			++iteration;
		}
		//завершение игры
		std::cout << "Game is over , press any key to close\n";
		system("pause");
	}


}
