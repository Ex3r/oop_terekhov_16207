#ifndef FACTORY_H
#define FACTORY_H
#include "../header.h"
#include "strategy.h"
#include "executor.h"
#include "parser.h"
#include "game.h"

class Factory
{
	
public:
	static controller::Strategy& createStrategy();
	static model::World& createWorld();
	static controller::Executor& createExecutor();
    static 	model::History& createHistory();
	static controller::Parser& createParser();
};



#endif // FACTORY_H
