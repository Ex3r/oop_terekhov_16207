#ifndef DESERIALIZER_H
#define DESERIALIZER_H
#include "../header.h"
#include "../model/cell.h"
namespace  controller
{
	class Deserializer
	{
	public:
		Deserializer() {};
		~Deserializer() {};
		/* 0 - �������� ��������*
		 * 1 - ������ ��� ��������*/
		static bool load(std::vector<std::vector<model::Cell>>& world, std::string fileName, std::string& output);
	};
}
#endif // DESERIALIZER_H
