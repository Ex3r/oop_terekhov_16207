#ifndef GAME_H
#define GAME_H
#include "strategy.h"
#include "factory.h"
namespace controller
{
	class Game
	{
		Strategy strategy;
		Parser parser;
		Executor executor;
		bool exit;
	public:
		Game(Strategy str, Parser par, Executor exec);
		void start();
		bool isGameEnd(size_t);
	};
}
#endif // GAME_H
