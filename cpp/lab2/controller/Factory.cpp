#include "factory.h"

//model::World* Factory::world = new model::World;
//controller::Executor* Factory::executor = new controller::Executor;
//model::History* Factory::history = new model::History;
//controller::Parser* Factory::parser = new controller::Parser;
//controller::Strategy* Factory::strategy = new controller::Strategy(*world, *history);


controller::Strategy& Factory::createStrategy() 
{
	static controller::Strategy strategy(createWorld(), createHistory());
	return strategy;
}

model::World& Factory::createWorld()
{
	static model::World world;
	return world;
}

controller::Executor& Factory::createExecutor()
{
	static controller::Executor executor;
	return executor;
}

model::History& Factory::createHistory()
{
	static model::History history;
	return history;
}

controller::Parser& Factory::createParser()
{
	static controller::Parser parser;
	return parser;
}


