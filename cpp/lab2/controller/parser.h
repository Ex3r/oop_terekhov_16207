#ifndef PARSER_H
#define PARSER_H
#include "../model/history.h"
#include "executor.h"

namespace controller
{
	static const std::string alphabet[] = { "a","b","c","d","e","f","g","h","i","j" };
	static const std::string possibleCommands[9] = { "reset", "set", "clear", "step", "back" ,"save", "load" , "help", "exit"};
	class Parser
	{
	public:
		model::CommandParameters parseCommand(std::string command);
	};
}
#endif // PARSER_H
