#ifndef CONSOLEVIEW_H
#define CONSOLEVIEW_H
#include "../header.h"
#include "../model/cell.h"

namespace view
{
	using Field = std::vector<std::vector<model::Cell>>;
	class ConsoleView
	{
	public:
		static void printField(Field& field);
		static void printStepCounter(size_t step);
		static void printHelp();
	};

}
#endif // CONSOLEVIEW_H
