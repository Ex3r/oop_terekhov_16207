#include "consoleView.h"
#include "../model/cell.h"
#include "../model/world.h"
void printAlphabet()
{
	static const  std::string alphabet[] = { "A","B","C","D","E","F","G","H","I","J" };
	std::cout << "  ";
	for (const std::string &i : alphabet) 
	{
		std::cout << i << " ";
	}
	std::cout << "\n";
}

void printCell(std::vector<model::Cell>::iterator iter)
{
	if ((*iter).getState() == true)
	{
		std::cout << "O ";
	}
	else
	{
		std::cout << ". ";
	}
}


void view::ConsoleView::printField(Field& field)
	{
	system("CLS");
	printAlphabet();
	for (size_t i = 0; i != field.size(); ++i)
	{
		std::cout << i << " ";
		for (std::vector<model::Cell>::iterator iter = (field[i]).begin(); iter != (field[i]).end(); ++iter)
		{
			printCell(iter);
		}
		std::cout << "\n";
	}
	
}

void view::ConsoleView::printStepCounter(size_t step)
{
	std::cout << "Step = " << step << "\n";

}

void view::ConsoleView::printHelp()
{
	const static std::string description = { "                         THE GAME OF life - Implementation in C++\n"
		"Also known simply as life, \n"
		"is a cellular automaton devised by the British mathematician John Horton Conway in 1970.\n"
		"Rules\n"
		"The universe of the Game of life is an infinite two-dimensional orthogonal grid of square cells,\n"
		"each of which is in one of two possible states, life or dead. Every\n"
		"cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent.\n"
		"At each step in time, the following transitions occur:\n"
		"1. Any live cell with fewer than two live neighbours dies, as if caused by under-population.\n"
		"2. Any live cell with two or three live neighbours lives on to the next generation.\n"
		"3. Any live cell with more than three live neighbours dies, as if by over-population.\n"
		"4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.\n"
		"\n"
		"O - living cell\n"
		". - dead cell\n"
		"       POSSIBLE COMMANDS:\n"
		"reset - clear field and step counter\n"
		"setXY - set cell into place on field (X from A to J , Y from 0 to 9\n"
		"clearXY - delete cell from place on field (X from A to J , Y from 0 to 9\n"
		"step N - scroll forward to the game for N steps\n"
		"back - scroll back the game for 1 step\n"
		"save \"filename\" - save the state of the game field to a text file in the current directory\n"
		"load \"filename\" - load the state of the game field from a text file in the current directory\n"
	};
	std::cout << description << std::endl;
	
}

