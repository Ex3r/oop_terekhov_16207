
#ifndef HISTORY_H
#define HISTORY_H

#include "../header.h"

namespace model
{
	/*���������� � ������*/
	struct CellModification
	{
		CellModification(unsigned x = 0, unsigned y = 0, bool state = false)
			:x(x), y(y), state(state) {}
		int x;
		int y;
		bool state;
	};
	using Storage = std::vector<std::list<CellModification>>;
	class History
	{
		std::shared_ptr<Storage> storage; 
	public:
		std::shared_ptr<Storage>& getStorage();
		void freeHistory() const;
		void writeToHistory(size_t stepCounter, CellModification CellModification) const;
		std::list<CellModification> readFromHistory(int index) const;
		History();
	};

}
#endif // HISTORY_H
