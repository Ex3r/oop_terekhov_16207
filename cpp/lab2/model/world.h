
#ifndef WORLD_H
#define WORLD_H

#include "../header.h"
#include "cell.h"

namespace  model
{
	using Field = std::vector<std::vector<Cell>>;

	class World
	{
		Field field;
		
	public:
		
		 const int width = 10;
		 const int height = 10;
		 World();
		 ~World();
		 Field& getField();
		 void clearField();

	};


}
#endif // WORLD_H
