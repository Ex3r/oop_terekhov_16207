#include "history.h"
std::shared_ptr<model::Storage>& model::History::getStorage()
{
	return storage;
}

void model::History::freeHistory() const
{
	storage->clear();
}

void model::History::writeToHistory(size_t stepCounter, CellModification CellModification) const
{
	if (storage->size() <= (stepCounter))
	{
		std::list<model::CellModification> insert;
		insert.push_back(CellModification);
		storage->push_back(insert);
		return;
	}

	(*storage)[stepCounter].push_back(CellModification);
	
}

std::list<model::CellModification> model::History::readFromHistory(int index) const
{
	std::list<model::CellModification> retList = storage->back();
	storage->pop_back();
	return retList;
}

model::History::History()
	:storage(new Storage())
{}

