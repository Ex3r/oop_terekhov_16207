﻿#ifndef CELL_H
#define CELL_H
namespace  model
{
	class Cell
	{
		bool state;
		int amountOfAliveNeigbours;
		bool hasChangedState;

	public:
		Cell();
		Cell(bool state);
		~Cell();

		int getState() const;
		int setState(bool state);

		int getAmountAliveNeigbours() const;
		void setAmountAliveNeigbours(int amount);

	};


}
#endif // CELL_H
