#include "cell.h"
namespace model
{
	Cell::Cell()
	{
		amountOfAliveNeigbours = 0;
		state = false;
		hasChangedState = false;
	}

	Cell::Cell(bool state = false)
		:state(state),
		amountOfAliveNeigbours(0),
		hasChangedState(false){}

	Cell::~Cell()
	{

	}

	int Cell::getState() const
	{
		if (state)
		{
			return true;
		}
		return false;
	}

	int Cell::setState(bool state) 
	{
		this->state = state;
		return state;
	}


	int Cell::getAmountAliveNeigbours() const
	{
		return amountOfAliveNeigbours;
	}

	void Cell::setAmountAliveNeigbours(int amount)
	{
		amountOfAliveNeigbours = amount;
	}
}
