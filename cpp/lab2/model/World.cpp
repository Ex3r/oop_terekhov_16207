#include "world.h"
namespace model
{
	Field& World::getField()
	{
		return field;
	}


	void model::World::clearField()
	{
		for (size_t i = 0; i != field.size(); ++i)
		{
			for (std::vector<model::Cell>::iterator iter = (field[i]).begin(); iter != (field[i]).end(); ++iter)
			{
				(*iter) = false;
			}
		}
	}

	World::World()
	:field(10)
	{
		for (size_t i = 0; i != field.size(); ++i)
		{
			field[i].resize(height);
		}
	}



	model::World::~World(){}
}

