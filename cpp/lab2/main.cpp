#include "controller/game.h"
#include "controller/factory.h"
#include "controller/serializer.h"

int main(int argc , char *argv[])
{
	using namespace controller;
	Factory f;
	Game game(f.createStrategy(),f.createParser(),f.createExecutor());
	game.start();	
	return 0;
}
	
