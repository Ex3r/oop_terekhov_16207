#include <gtest/gtest.h>
#include "../lab3[Workflow]/readfileWorker.h"
#include "../lab3[Workflow]/grepWorker.h"
#include "../lab3[Workflow]/sortWorker.h"

class SortWorkerTest : public testing::Test
{

};

TEST_F(SortWorkerTest, testSort_1)
{
	const std::string filename = "testFiles/testSort.txt";
	std::string output{};
	std::list<std::string> rawFile;
	ReadWorker reader(filename);
	reader.process(rawFile, output);
	SortWorker sort;
	sort.process(rawFile, output);
	EXPECT_EQ(rawFile.front(), "aaa");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.front(), "bbb");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.front(), "ccc");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.front(), "zzz");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.size(), 0);

}