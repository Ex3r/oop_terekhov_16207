#include <gtest/gtest.h>
#include "../lab3[Workflow]/readfileWorker.h"

class ReadFileWorkerTest : public testing::Test
{
	
};

TEST_F(ReadFileWorkerTest, testRead_correct_file)
{
	const std::string filename = "testFiles/testRead_2.txt";
	std::string output;
	std::list<std::string> rawFile;
	ReadWorker reader(filename);
	reader.process(rawFile,output);
	std::string s1 = rawFile.front();

	EXPECT_EQ(rawFile.front(), "aaa");
	EXPECT_EQ(rawFile.back(), "eee");

}

TEST_F(ReadFileWorkerTest, testRead_file_no_exist)
{
	std::string filename = "testFiles/something.txt";
	std::string output;
	std::list<std::string> rawFile;
	ReadWorker reader(filename);
	reader.process(rawFile,output);
	EXPECT_EQ(rawFile.size(), 0);
	EXPECT_EQ(output, "Can't open file testFiles/something.txt");
	
}
