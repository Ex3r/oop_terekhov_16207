#include <gtest/gtest.h>
#include "../lab3[Workflow]/ParserUtils.h"

class ParserTest : public testing::Test
{

};

TEST_F(ParserTest, testParser_missedLabel)
{
	const std::string filename = "testFiles/testParser_missedLabel.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}

TEST_F(ParserTest, testParser_incorrectLabel)
{
	const std::string filename = "testFiles/testParser_incorrectLabel.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}

TEST_F(ParserTest, testParser_minusId)
{
	const std::string filename = "testFiles/testParser_minusId.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}

TEST_F(ParserTest, testParser_repeatedId)
{
	const std::string filename = "testFiles/testParser_repeatedId.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_readingMissingEualSign)
{
	const std::string filename = "testFiles/testParser_readingMissingEualSign.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_unknownWorker)
{
	const std::string filename = "testFiles/testParser_unknownWorker.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}

TEST_F(ParserTest, testParser_notEnoughArgumentsForCommand)
{
	const std::string filename = "testFiles/testParser_notEnoughArgumentsForCommand.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}

TEST_F(ParserTest, testParser_noScheme)
{
	const std::string filename = "testFiles/testParser_noScheme.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_missedClosedLabel)
{
	const std::string filename = "testFiles/testParser_missedClosedLabel.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_testParser_incorrectClosingLabel)
{
	const std::string filename = "testFiles/testParser_incorrectClosingLabel.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_testParser_missedArgAtScheme)
{
	const std::string filename = "testFiles/testParser_missedArgAtScheme.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_testParser_missedConnectionSign)
{
	const std::string filename = "testFiles/testParser_missedConnectionSign.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_noReadfileFirst)
{
	const std::string filename = "testFiles/testParser_noReadfileFirst.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_noWritefileLast)
{
	const std::string filename = "testFiles/testParser_noWritefileLast.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
TEST_F(ParserTest, testParser_ExtraArgumentsAtScheme)
{
	const std::string filename = "testFiles/testParser_ExtraArgumentsAtScheme.txt";
	Parser parser(filename);
	ASSERT_ANY_THROW(parser.parse());

}
