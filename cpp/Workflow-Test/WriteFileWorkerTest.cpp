#include <gtest/gtest.h>
#include "../lab3[Workflow]/writefileWorker.h"
#include "../lab3[Workflow]/readfileWorker.h"

class WriteFileWorkerTest : public testing::Test
{

};

TEST_F(WriteFileWorkerTest, testWrite_correct_file)
{
	const std::string filename = "testFiles/testWrite.txt";
	std::string output{"y"};
	std::list<std::string> rawFile;
	rawFile.push_back("test1");
	rawFile.push_back("test2");
	WriteWorker writer(filename);
	writer.process(rawFile, output);
	EXPECT_EQ(output, "");
	std::list<std::string> read;
	ReadWorker reader(filename);
	reader.process(read, output);
	EXPECT_EQ(output, "");
	EXPECT_EQ(rawFile.front(), read.front());
	EXPECT_EQ(rawFile.back(), read.back());

}


TEST_F(WriteFileWorkerTest, testWrite_correct_file_rewrite_no)
{
	const std::string filename = "testFiles/testRewrite.txt";
	std::string output{ "n" };
	std::list<std::string> rawFile;
	rawFile.push_back("test1_12212");
	rawFile.push_back("test2_12121314");
	WriteWorker writer(filename);
	writer.process(rawFile, output);
	EXPECT_EQ(output, "Failure to overwrite a file testFiles/testRewrite.txt");
	std::list<std::string> read;
	ReadWorker reader(filename);
	reader.process(read, output);
	EXPECT_EQ(rawFile.front(), read.front());
	EXPECT_EQ(rawFile.back(), read.back());

}