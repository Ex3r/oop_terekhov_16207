#include <gtest/gtest.h>
#include "../lab3[Workflow]/readfileWorker.h"
#include "../lab3[Workflow]/replaceWorker.h"


class ReplaceWorkerTest : public testing::Test
{

};

TEST_F(ReplaceWorkerTest, testReplace)
{
	const std::string filename = "testFiles/testReplace.txt";
	std::string output{};
	const std::string oldWord{ "a" };
	const std::string newWord{ "bla" };

	std::list<std::string> rawFile;
	ReadWorker reader(filename);
	reader.process(rawFile, output);
	ReplaceWorker replace(oldWord,newWord);
	replace.process(rawFile, output);

	EXPECT_EQ(rawFile.front(), "blalblaskbla");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.front(), "blapple");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.front(), "grunt");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.front(), "cblat");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.size(), 0);

}