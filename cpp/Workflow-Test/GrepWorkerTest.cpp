#include <gtest/gtest.h>
#include "../lab3[Workflow]/readfileWorker.h"
#include "../lab3[Workflow]/grepWorker.h"

class GrepWorkerTest : public testing::Test
{

};

TEST_F(GrepWorkerTest, testWrite_correct_file)
{
	const std::string filename = "testFiles/testGrep.txt";
	std::string output{};
	std::string word{"�������"};
	std::list<std::string> rawFile;
	ReadWorker reader(filename);
	reader.process(rawFile, output);
	GrepWorker grep(word);
	grep.process(rawFile, output);
	
	EXPECT_EQ(rawFile.front(),"�������");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.front(), "�������123");
	rawFile.pop_front();
	EXPECT_EQ(rawFile.size(), 0);

}